import { Router } from 'express';
import {
    buscarSinTerminarIb,
    llamadaSinIdAsteriskIb,
    finLlamadaib, getCotizaciones,
    guardarSubetiqueta,
    inicioLlamadaIb, llamadaSinEtiquetaIb,
    setAsteriskIds, sinTerminarIbCDR, llamadaSinFinalizar
} from "../../controllers/llamada-entrantes/LlamadaEntrantes";

const router = Router();

router.post("/", inicioLlamadaIb);
router.put('/setasteriskids/:numero/:idLlamada', setAsteriskIds);
router.put('/fin-llamada-ib/:idLlamada', finLlamadaib);
router.put("/subetiqueta/:idLlamada", guardarSubetiqueta);
router.get("/cotizaciones/:numero", getCotizaciones);
router.get("/llamada-sin-etiqueta/:idEmpleado", llamadaSinEtiquetaIb);
router.get("/buscar-sin-terminar/:idAsterisk", buscarSinTerminarIb);
router.put("/fin-sin-asterisk/:idLlamada", llamadaSinIdAsteriskIb);
router.get('/sin-terminar-cdr-entrantes/:idAsterisk', sinTerminarIbCDR);
router.put('/llamada-ib-no-finalizada/:idEmpleado', llamadaSinFinalizar);

export default router;
