import { Router } from 'express';
import {
    getLlamadasByEMpleado,
    getLlamadasIbBySubarea, getLlamadasPjsip, getLlamadasSipPorEmpleado,
} from "../../../controllers/llamada-entrantes/view/llamadaEntrantesViewController";
const router = Router();

router.get("/:idEmpleado", getLlamadasByEMpleado);
router.get("/ib-subarea/:idUsuario/:idPuesto/:idSubarea/:idEmpleado", getLlamadasIbBySubarea);
router.get("/llamada-in-sip/:idUsuario/:idPuesto/:idSubarea/:idEmpleado", getLlamadasSipPorEmpleado);
router.get("/pjsip-in/:idUsuario/:idPuesto/:idSubarea/:idEmpleado", getLlamadasPjsip);

export default router;
