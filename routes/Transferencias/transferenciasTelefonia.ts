import { Router } from 'express';
import {getNumeros} from "../../controllers/transferenciaTelefonia/transferenciaTelefoniaController";

const router = Router();

router.get("/", getNumeros);

export default router;
