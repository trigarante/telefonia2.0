import { Router } from 'express';
import {getAseguradorasGmm} from "../../controllers/GMM/GmmController";
const router = Router();
router.get('/aseguradoras', getAseguradorasGmm);

export default router;
