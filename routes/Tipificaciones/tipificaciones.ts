import { Router } from 'express';
import {
    getEstadoSolicitud, getEstadoSolicitudAgenda,
    getEtiquetaSolicitud,
    getSubEtiquetaSolicitud,
    getEstadoSolicitudReno,
    getEstadoSolicitudGm, getEstadoSolicitudCobranza
} from '../../controllers/Tipificaciones/tipificacionesController';

const router = Router();

router.get('/estado-solicitud', getEstadoSolicitud);
router.get('/estado-solicitud-reno', getEstadoSolicitudReno);
router.get('/estado-solicitud-gm', getEstadoSolicitudGm);
router.get('/estado-solicitud-cobranza', getEstadoSolicitudCobranza);
router.get('/etiqueta-solicitud/:idEstadoSolicitud', getEtiquetaSolicitud);
router.get('/subetiqueta/:idEtiqueta', getSubEtiquetaSolicitud);
router.get('/estados/agenda', getEstadoSolicitudAgenda);

export default router;
