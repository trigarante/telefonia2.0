import { Router } from 'express';
import {getCallCenters} from "../../controllers/llamada-simultanea/llamadaSimultaneaController";

const router = Router();

router.get('/call-centers/:idPuesto', getCallCenters);

export default router;
