import { Router } from 'express';
import {
    buzonGrabacion, buzonGrabacionPjsip, getArchivoPjsip,
    getGrabacionOutbound,
    getNombreArchivo
} from "../../controllers/Grabaciones/GrabacionesController";


const router = Router();

router.get('/', getGrabacionOutbound);
router.get("/nombreArchivo/:idAsterisk", getNombreArchivo);
router.get('/grabaciones/buzon/:oldIdAsterisk/:numero', buzonGrabacion);
router.get('/archivo-pjsip/:idAsterisk', getArchivoPjsip);
router.get('/buzon-pjsip/:oldIdAsterisk/:numero', buzonGrabacionPjsip);


export default router;
