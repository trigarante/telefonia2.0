import {Router} from "express";
import {
    finLlamadaIb, guardarSubetiquetaIb,
    inicioLlamadaEntrante,
    setIdAsteriskIb
} from "../../../controllers/pjsip/llamadaEntrantes/llamadaEntrantesController";
const router = Router();

router.post('/', inicioLlamadaEntrante);
router.put('/idAsterisk/:numero/:idLlamada', setIdAsteriskIb);
router.put('/finllamadaib/:idLlamada', finLlamadaIb);
router.put('/subetiqueta/:idLlamada', guardarSubetiquetaIb);

export default router;
