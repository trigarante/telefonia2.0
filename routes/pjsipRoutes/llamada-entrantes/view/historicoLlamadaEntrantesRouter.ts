import {Router} from "express";
import {
    getAllLlamadasIb,
    getLlamadasByIdEmpleado
} from "../../../../controllers/pjsip/llamadaEntrantes/views/historicoLlamadaEntrantesController";

const router = Router();

router.get('/', getAllLlamadasIb);
router.get('/:idEmpleado', getLlamadasByIdEmpleado);

export default router;
