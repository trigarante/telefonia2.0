import {Router} from "express";
import llamadaSalidasRoutes from '../../routes/pjsipRoutes/llamada-salidas/llamada-salidas-routes';
import LlamadaEntrantesRoutes from '../../routes/pjsipRoutes/llamada-entrantes/llamadaEntrantesRoutes';
import historicoLlamadasRoutes from '../pjsipRoutes/historico-llamadas/historicoLlamadasRoutes';
import monitoreoRoutes from '../pjsipRoutes/monitoreo/monitoreoRoutes';
import tipificacionesRoutes  from '../pjsipRoutes/Tipificaciones/tipificacionesRoutes';
const router = Router();

router.use('/llamada-salidas' , llamadaSalidasRoutes);
router.use('/llamada-entrantes', LlamadaEntrantesRoutes);
router.use('/historico-llamadas', historicoLlamadasRoutes);
router.use('/monitoreo', monitoreoRoutes);
router.use('/tipificaciones', tipificacionesRoutes);

export default router;
