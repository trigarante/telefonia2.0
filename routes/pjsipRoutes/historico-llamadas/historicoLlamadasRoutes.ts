import {Router} from "express";
import historicoLlamadaDeSalida from '../llamada-salidas/views/historicoLlamadaSalidasRoutes';
import historicoLlamadaDeEntrada from '../llamada-entrantes/view/historicoLlamadaEntrantesRouter';
const router = Router();

router.use('/salida', historicoLlamadaDeSalida);
router.use('/entrada', historicoLlamadaDeEntrada);

export default router;
