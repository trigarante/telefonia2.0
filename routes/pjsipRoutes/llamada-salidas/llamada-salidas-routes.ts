import {Router} from "express";
import {
    finLlamada, finLlamadaIntermitencia,
    inicioLlamada, setAsteriskId,
    tipificarLlamada
} from "../../../controllers/pjsip/llamadaSalidas/llamadaSalidasController";
import {
    agregarNumero,
    getNumberByIdSolicitud
} from "../../../controllers/pjsip/llamadaSalidas/numerosAdicionalesController";
const router = Router();

router.post('/', inicioLlamada);
router.put('/:idLlamada', finLlamada);
router.put('/tipificacion/:idLlamada', tipificarLlamada);
router.put('/idAsterisk/:numero/:idLlamada', setAsteriskId);
router.put('/finconintermitencia/:idLlamada', finLlamadaIntermitencia);
router.post('/add-number', agregarNumero);
router.get('/numeroByidSolicitud/:idSolicitud', getNumberByIdSolicitud);
export default router;
