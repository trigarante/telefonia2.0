import {Router} from "express";
import {getLlamadasByIdSOlicitud} from "../../../../controllers/pjsip/llamadaSalidas/views/historicoLlamadaSalidasController";

const router = Router();

router.get('/:idSolicitud', getLlamadasByIdSOlicitud);

export default router;
