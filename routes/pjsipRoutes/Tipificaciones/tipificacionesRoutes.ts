import {Router} from "express";
import {
    getEstados,
    getEtiquetas,
    getSubEtiquetas, sinTipificarIn, sinTipificarOut,
} from "../../../controllers/pjsip/Tipificaciones/tipificacionesController";
const router = Router();

router.get('/', getEstados);
router.get('/:idEstadoTipificacion', getEtiquetas);
router.get('/subetiqueta/:idEtiquetaLlamada', getSubEtiquetas);
router.get('/sin-tipificar-in/:idEmpleado', sinTipificarIn);
router.get('/sin-tipificar-out/:idEmpleado', sinTipificarOut);

export default router;
