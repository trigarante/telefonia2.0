import {Router} from "express";
import {getLlamadasEnProceso} from "../../../controllers/pjsip/llamadaSalidas/views/llamadaOutProcesoController";
import {getLlamadaInEnProceso} from "../../../controllers/pjsip/llamadaEntrantes/views/llamadaInProcesoController";
import {buscarSinTerminar} from "../../../controllers/pjsip/llamadaSalidas/llamadaSalidasController";


const router = Router();

router.get('/llamadas-out/:idUsuario/:idPuesto/:idTipo/:idSubarea', getLlamadasEnProceso);
router.get('/llamadas-in/:idUsuario/:idPuesto/:idTipo/:idSubarea', getLlamadaInEnProceso);
router.get('/llamada-finalizada/:idAsterisk', buscarSinTerminar);


export default router;
