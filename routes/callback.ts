import { Router } from 'express';
import {postCallback, postCbPeru} from "../controllers/llamada-salidas/LlamadaSalidasController";
import {
    ejecutivosLinea, ejecutivosLineaReno,
    empleadoEnLlamadaIn, empleadoEnLlamadaReno,
    getSegLlamada, getSegLlamadaReno
} from "../controllers/Monitoreo/SeguimientoLlamadaController";

const router = Router();

router.post("/llamadasalida/callback", postCallback);
router.post("/llamadasalida/perucb", postCbPeru);
router.get("/monitoreo", getSegLlamada);
router.get("/monitoreo-reno", getSegLlamadaReno);
router.get("/supervisores/ejecutivos-linea/in", ejecutivosLinea);
router.get("/supervisores/ejecutivos-linea/reno", ejecutivosLineaReno);
router.get("/supervisores/empleado-en-llamada-in", empleadoEnLlamadaIn);
router.get("/supervisores/empleado-en-llamada-reno", empleadoEnLlamadaReno);

export default router;
