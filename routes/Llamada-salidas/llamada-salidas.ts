import { Router } from 'express';
import {
    inicioLlamada,
    finLlamada,
    guardarSubetiqueta,
    modificarSolicitud,
    setAsteriskId,
    postCallback,
    inicioCallback,
    endCallback,
    getLlamadaCallback,
    llamadaSinEtiqueta, findCallbackByIdSolicitud, buscarSinTerminar, llamadaSinidAsterik, sinTerminarByCDR,
} from '../../controllers/llamada-salidas/LlamadaSalidasController';

const router = Router();

router.post("/", inicioLlamada);
router.post("/callback", postCallback);
router.put("/start-callback/:idLlamada", inicioCallback);
router.put("/end-callback/:idLlamada", endCallback);
router.get("/llamada-pendiente/:idEmpleado", getLlamadaCallback);
router.put("/:idLlamada", finLlamada);
router.put("/subetiqueta/:idLlamada", guardarSubetiqueta);
router.put("/solicitud/:idSolicitud", modificarSolicitud);
router.put("/idAsterisk/:numero/:idLlamada", setAsteriskId);
router.get("/llamada-sin-etiqueta/:idEmpleado", llamadaSinEtiqueta);
router.get("/callback-solicitud/:idSolicitud/:idEmpleadoViejo/:idEmpleadoNuevo", findCallbackByIdSolicitud);
router.get("/buscar-sin-terminar/:idAsterisk", buscarSinTerminar);
router.put("/fin-sin-asterisk/:idLlamada", llamadaSinidAsterik);
router.get("/sin-terminar-cdr/:idAsterisk", sinTerminarByCDR);

export default router;
