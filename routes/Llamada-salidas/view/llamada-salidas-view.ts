import { Router } from 'express';
import {
    getLlamadasByIdRegistro,
    getLlamadasByIdSolicitud, getLlamadasPorSolicitudPjsip, llamadasporIdRegistroPjsip
} from "../../../controllers/llamada-salidas/view/llamadaSalidasViewController";

const router = Router();

router.get("/llamadasByIdSolicitud/:idSolicitud", getLlamadasByIdSolicitud);
router.get("/llamadasIdRegistro/:idRegistro", getLlamadasByIdRegistro);
router.get("/pjsip/llamadas-out/:idSolicitud", getLlamadasPorSolicitudPjsip);
router.get("/pjsip/llamadasout-idRegistro/:idRegistro", llamadasporIdRegistroPjsip);

export default router;
