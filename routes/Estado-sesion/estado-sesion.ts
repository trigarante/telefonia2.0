import { Router } from 'express';
import {
    getEstados,
    getMotivosPausaByIdTipoPausa
} from "../../controllers/Estado-sesion/estado-sesionController";

const router = Router();

router.get("/", getEstados);
router.get("/motivos-pausa/:idTipoPausa", getMotivosPausaByIdTipoPausa);
export default router;
