import { Router } from 'express';
import llamadaSalidas from '../routes/Llamada-salidas/llamada-salidas';
import llamadaEntrantes from '../routes/Llamada-entrantes/llamada-entrantes'
import tipificaciones from '../routes/Tipificaciones/tipificaciones';
import llamadasSalidaView from '../routes/Llamada-salidas/view/llamada-salidas-view';
import grabaciones from '../routes/Grabaciones/grabaciones';
import solicitudes from '../routes/Solicitudes/solicitudes';
import speech from '../routes/Speech/speech';
import estadoSesion from '../routes/Estado-sesion/estado-sesion';
import calidad from '../routes/Calidad/calidad';
import llamadaEntrantesView from '../routes/Llamada-entrantes/view/llamadaEntrantesView';
import supervisores from '../routes/Supervisores/supervisores';
import llamadaSimultanea from '../routes/Llamada-simultanea/llamada-simultanea';
import finanzas from '../routes/Finanzas/finanzas';
import auditoria  from  '../routes/Auditoria/auditoria';
import gastosMedicos from '../routes/GMM/gastosMedicos';
import transferencias from '../routes/Transferencias/transferenciasTelefonia';
import {getCurrentServer} from "../controllers/currentServer/currentServerController";

const router = Router();

router.use("/llamada-salidas", llamadaSalidas);
router.use("/tipificaciones",tipificaciones);
router.use("/llamadaSalidasView",llamadasSalidaView);
router.use('/recordings', grabaciones);
router.use('/solicitudes', solicitudes);
router.use('/speech', speech);
router.use('/llamada-entrantes', llamadaEntrantes);
router.use('/estado-sesion', estadoSesion);
router.use('/calidad', calidad);
router.use("/llamada-in", llamadaEntrantesView);
router.use("/supervisores", supervisores);
router.use("/llamada-simultanea", llamadaSimultanea);
router.use("/finanzas",finanzas);
router.use("/auditoria", auditoria);
router.use("/gastos-medicos", gastosMedicos);
router.use("/transferencias", transferencias);
router.use("/currentServer", getCurrentServer);

export default router;
