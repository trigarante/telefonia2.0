import { Router } from 'express';
import {getSpeech} from "../../controllers/Speech/speechController";
const router = Router();

router.get("/bySubarea/:idSubarea", getSpeech);
export default router;
