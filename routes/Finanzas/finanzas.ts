import { Router } from 'express';
import { aplicacionesById } from '../../controllers/Finanzas/aplicaciones';

const router = Router();

router.get("/aplicaciones/:id", aplicacionesById);

export default router;
