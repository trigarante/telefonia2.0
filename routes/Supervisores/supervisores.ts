import { Router } from 'express';

import {
    enLinea,
    getenLlamadaEntrada,
    enPausa,
    getenLlamadaSalida,
    agregarNumeroRegistro,
    getNumerosByIdRegistro,
    callbackRealizado,
    empleadoCallback,
    getEmpleadosCallback,
    getCallbackEmpleado,
    getNumerosByIdSolicitud,
    enLlamadaSalidaPjsip
} from "../../controllers/Supervisores/supervisoresController";


const router = Router();

router.get("/out-call/:idUsuario/:idPuesto/:idTipo/:idSubarea", getenLlamadaSalida);
router.get("/inbound-call/:idUsuario/:idPuesto/:idTipo/:idSubarea", getenLlamadaEntrada);
router.get("/en-linea/:idUsuario", enLinea);
router.get("/pausa/:idUsuario", enPausa);
router.post("/add-number-registro", agregarNumeroRegistro);
router.get("/get-numeros/:idRegistro", getNumerosByIdRegistro);
router.get("/contactos/:idSolicitud", getNumerosByIdSolicitud);
router.get("/callback-realizado/:empleado", callbackRealizado);
router.put("/empleado-callback/:idEmpleado/:idLlamada", empleadoCallback);
router.get("/callback-totales/:id/:idPuesto/:idTipo/:idSubarea", getEmpleadosCallback);
router.get("/callback-empleados/:empleado", getCallbackEmpleado);
router.get('/pjsip-out-call/:idUsuario/:idPuesto/:idTipo/:idSubarea', enLlamadaSalidaPjsip);
export default router;
