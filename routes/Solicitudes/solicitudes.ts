import { Router } from 'express';
import {getSolicitud} from "../../controllers/SolicitudesVn/solicitudesController";

const router = Router();

router.get("/get-solicitud/:idSolicitud", getSolicitud);


export default router;
