import { Router } from 'express';
import {getllamadasEntrantesByDate, getLlamadasSalidaByDate} from "../../controllers/Calidad/calidadController";

const router = Router();

router.get('/salidas', getLlamadasSalidaByDate);
router.get('/entrantes', getllamadasEntrantesByDate);


export default router;
