import {Router} from "express";
import {getCurrentServer} from "../../controllers/currentServer/currentServerController";

const router = Router();
router.get("/", getCurrentServer);

export default router;
