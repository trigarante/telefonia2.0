import { Router } from 'express';
import {
    crearSpeech,
    getSpeechById,
    getSpeechCreados,
    speechPorAutorizar,
    getSpeechAutorizado,
    getSpeechInactivos,
    getSpeechInhabilitados,
    bajaSpeech,
    putSpeech,
    getSpeechBySubarea,
    putAutorizar,
    putDesautorizar,
    putInhabilitar
} from "../../controllers/Auditoria/auditoriaController";
const router = Router();

router.get('/speech', getSpeechCreados);
router.post('/speech/crearSpeech', crearSpeech);
router.get('/speech/porAutorizar', speechPorAutorizar);
router.get('/speech/byIdSpeech/:idSpeech', getSpeechById);
router.get('/speech/inactivos', getSpeechInactivos);
router.get('/speech/autorizados', getSpeechAutorizado);
router.get('/speech/inhabilitados', getSpeechInhabilitados);
router.put('/speech/bajaSpeech/:idSpeech', bajaSpeech);
router.put('/speech/actualizarSpeech', putSpeech);
router.get('/speech/bySubarea/:idSubarea', getSpeechBySubarea);
router.put('/speech/autorizar', putAutorizar);
router.put('/speech/desautorizar', putDesautorizar);
router.put('/speech/inhabilitar', putInhabilitar);

export default router;
