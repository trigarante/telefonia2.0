import { DataTypes } from 'sequelize';
import  {db} from "../../../db/connection";

const NumerosAdicionales = db.define('numerosAdicionales', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idSolicitud: {
        type: DataTypes.INTEGER
    },
    numero: {
        type: DataTypes.STRING
    }
});

export default NumerosAdicionales;
