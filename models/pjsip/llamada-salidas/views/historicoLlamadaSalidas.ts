import { DataTypes } from 'sequelize';
import  {db} from "../../../../db/connection";

const historicoLlamadaSalidas = db.define('historicoLlamadaSalidasGo', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaFinal: {
        type: DataTypes.DATE,
    },
    idSolicitud: {
        type: DataTypes.INTEGER,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
    recordingfile: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING,
    },
    tipoServer: {
        type: DataTypes.TINYINT,
    }
});

export default historicoLlamadaSalidas;

