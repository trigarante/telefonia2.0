import { DataTypes } from 'sequelize';
import  {db} from "../../../../db/connection";

const EnLlamadaOutPjsip = db.define('enLlamadaOutPjsip', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
});


export default EnLlamadaOutPjsip;
