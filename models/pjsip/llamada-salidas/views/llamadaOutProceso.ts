import { DataTypes } from 'sequelize';
import  {db} from "../../../../db/connection";

const LlamadaOutProceso = db.define('enLlamadaSalidaGoView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    numero: {
        type: DataTypes.STRING,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
});

export default LlamadaOutProceso;

