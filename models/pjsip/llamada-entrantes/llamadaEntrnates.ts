import { DataTypes } from 'sequelize';
import  {db} from "../../../db/connection";

const llamadaEntrantesGo = db.define('llamadaEntrantesGo', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idTipoLlamada: {
        type: DataTypes.INTEGER,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaFinal: {
        type: DataTypes.DATE,
    },
    idEstadoLlamada: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    idSubEtiqueta: {
        type: DataTypes.INTEGER,
    },
    idSolicitud: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.STRING,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    finTipificacion: {
        type: DataTypes.DATE,
    },
    tipoServer: {
        type: DataTypes.TINYINT,
    },
});

export default llamadaEntrantesGo;
