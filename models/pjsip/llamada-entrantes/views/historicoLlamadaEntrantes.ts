import { DataTypes } from 'sequelize';
import  {db} from "../../../../db/connection";

const historicoLlamadaEntantes = db.define('historicoLlamadaEntrantesGo', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaFinal: {
        type: DataTypes.DATE,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    recordingfile: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING,
    },
    tipoServer: {
        type: DataTypes.TINYINT,
    },
});

export default historicoLlamadaEntantes;

