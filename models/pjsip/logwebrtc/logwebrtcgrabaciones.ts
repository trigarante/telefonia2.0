import { DataTypes } from 'sequelize';
import { dbpjsip } from '../../../db/connection';

const LogwebrtcGrabaciones = dbpjsip.define('logwebrtcgrabaciones', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    uniqueid: {
        type: DataTypes.STRING,
    },
    calldate: {
        type: DataTypes.DATE,
    },
    src: {
        type: DataTypes.STRING,
    },
    recordingfile: {
        type: DataTypes.STRING,
    },
    park: {
        type: DataTypes.STRING,
    }
});

export default LogwebrtcGrabaciones;
