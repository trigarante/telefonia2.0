import { DataTypes } from 'sequelize';
import { db } from '../../../db/connection';

const SubEtiquetaLlamada = db.define('subEtiquetaLlamada', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    idEtiquetaLlamada: {
        type: DataTypes.INTEGER,
    },
    activo: {
        type: DataTypes.TINYINT,
    }
});

export default SubEtiquetaLlamada;
