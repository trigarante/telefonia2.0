import { DataTypes } from 'sequelize';
import { db } from '../../../db/connection';

const EtiquetaLlamada = db.define('etiquetaLlamada', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    idEstadoTipificacion: {
        type: DataTypes.INTEGER,
    },
    activo: {
        type: DataTypes.TINYINT,
    }
});

export default EtiquetaLlamada;
