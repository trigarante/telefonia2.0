import { DataTypes } from 'sequelize';
import { db } from '../../../db/connection';

const EstadoTipificacion = db.define('estadoTipificacion', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.TINYINT,
    }
});

export default EstadoTipificacion;
