import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';

const aplicacionesView = db.define('aplicacionesView', {
    id: {
        type: DataTypes.NUMBER,
        primaryKey: true,
    },
    idEmpleado: {
        type: DataTypes.NUMBER,
    },
    idProducto: {
        type: DataTypes.NUMBER,
    },
    idTipoPago: {
        type: DataTypes.NUMBER,
    },
    idProductoSocio: {
        type: DataTypes.NUMBER,
    },
    idFlujoPoliza: {
        type: DataTypes.NUMBER,
    },
    poliza: {
        type: DataTypes.STRING,
    },
    fechaInicio: {
        type: DataTypes.STRING
    },
    fechaRegistro: {
        type: DataTypes.STRING
    },
    primaNeta: {
        type: DataTypes.NUMBER
    },
    archivo: {
        type: DataTypes.STRING
    },
    cantidadPagos: {
        type: DataTypes.NUMBER
    },
    tipoPago: {
        type: DataTypes.STRING
    },
    productoSocio: {
        type: DataTypes.STRING
    },
    datos: {
        type: DataTypes.NUMBER
    },
    idEstadoPoliza: {
        type: DataTypes.NUMBER
    },
    estado: {
        type: DataTypes.STRING
    },
    idArea: {
        type: DataTypes.NUMBER
    },
    nombre: {
        type: DataTypes.STRING
    },
    apellidoPaterno: {
        type: DataTypes.STRING
    },
    apellidoMaterno: {
        type: DataTypes.STRING
    },
    idSede: {
        type: DataTypes.NUMBER
    },
    idGrupo: {
        type: DataTypes.NUMBER
    },
    idPuesto: {
        type: DataTypes.NUMBER
    },
    idEmpresa: {
        type: DataTypes.NUMBER
    },
    idSubarea: {
        type: DataTypes.NUMBER
    },
    descripcion: {
        type: DataTypes.STRING
    },
    numeroSerie: {
        type: DataTypes.STRING
    },
    subarea: {
        type: DataTypes.NUMBER
    },
    idPais: {
        type: DataTypes.NUMBER
    },
    idCliente: {
        type: DataTypes.NUMBER
    },
    idSolictud: {
        type: DataTypes.NUMBER
    },
    nombreCliente: {
        type: DataTypes.STRING
    },
    apellidoPaternoCliente: {
        type: DataTypes.STRING
    },
    apellidoMaternoCliente: {
        type: DataTypes.STRING
    },
    correo: {
        type: DataTypes.STRING
    },
    telefonoMovil: {
        type: DataTypes.STRING
    },
    carpetaCliente: {
        type: DataTypes.STRING
    },
    idRecibo: {
        type: DataTypes.NUMBER
    },
    idEstadoRecibo: {
        type: DataTypes.NUMBER
    },
    estadoRecibo: {
        type: DataTypes.STRING
    },
    nombreComercial: {
        type: DataTypes.STRING
    },
    tipoSubRamo: {
        type: DataTypes.STRING
    },
    idSocio: {
        type: DataTypes.NUMBER
    },
    estadoPago: {
        type: DataTypes.STRING
    }
});
export default aplicacionesView;
