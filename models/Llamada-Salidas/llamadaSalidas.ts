import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const llamadaSalidas = db.define('llamadaSalidas' ,{
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idTipoLlamada: {
        type: DataTypes.INTEGER,
    },
    idEstadoLlamada: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    idSubEtiqueta: {
        type: DataTypes.INTEGER,
    },
    idSolicitud: {
        type: DataTypes.INTEGER,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaFinal: {
        type: DataTypes.DATE,
    },
    numero: {
        type: DataTypes.STRING,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    finTipificacion: {
        type: DataTypes.DATE,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
});


export default llamadaSalidas;
