import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';

const LlamadaSalidasPjsipView = db.define('llamadaSalidasPjsipView', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    idSolicitud: {
        type: DataTypes.BIGINT,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    idRegistro: {
        type: DataTypes.BIGINT,
    },
    numero: {
        type: DataTypes.STRING,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    subetiqueta: {
        type: DataTypes.STRING,
    },
    idTipoServer: {
        type: DataTypes.TINYINT,
    },
});

export default LlamadaSalidasPjsipView;
