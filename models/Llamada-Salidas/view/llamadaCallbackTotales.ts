import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';

const llamadaCallbackTotales = db.define('llamadaCallbackTotales', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    idSolicitud: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
});

export default llamadaCallbackTotales;
