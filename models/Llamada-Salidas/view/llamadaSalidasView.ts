import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';

const llamadaSalidasView = db.define('llamadaSalidasView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    idSolicitud: {
        type: DataTypes.INTEGER,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    apellidoPaterno: {
        type: DataTypes.STRING,
    },
    apellidoMaterno: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
    subetiqueta: {
        type: DataTypes.STRING,
    },
});

export default llamadaSalidasView;
