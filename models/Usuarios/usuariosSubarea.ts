import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const usuariosSubarea = db.define('usuariosSubarea', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idUsuario: {
        type: DataTypes.INTEGER,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    comentarios: {
        type: DataTypes.STRING,
    },
});

export default usuariosSubarea;
