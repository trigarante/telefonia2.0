import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';
const usuarios = db.define('usuarios', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idCorreo: {
        type: DataTypes.INTEGER,
    },
    idGrupo: {
        type: DataTypes.INTEGER,
    },
    idTipo: {
        type: DataTypes.INTEGER,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    usuario: {
        type: DataTypes.STRING,
    },
    estado: {
        type: DataTypes.INTEGER,
    },
    imagenURL: {
        type: DataTypes.STRING,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
});

export default usuarios;
