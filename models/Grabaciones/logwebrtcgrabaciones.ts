import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const logwebrtcgrabaciones  = db.define('logwebrtcgrabaciones', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    uniqueid: {
        type: DataTypes.STRING,
    },
    calldate: {
        type: DataTypes.DATE,
    },
    src: {
        type: DataTypes.STRING,
    },
    recordingfile: {
        type: DataTypes.STRING,
    },
    park: {
        type: DataTypes.STRING,
    }
});


export default logwebrtcgrabaciones;
