import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const logwebrtcgrabacionesold  = db.define('logwebrtcgrabacionesold', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    uniqueid: {
        type: DataTypes.STRING,
    },
    calldate: {
        type: DataTypes.DATE,
    },
    src: {
        type: DataTypes.STRING,
    },
    recordingfile: {
        type: DataTypes.STRING,
    },
    park: {
        type: DataTypes.STRING,
    }
});


export default logwebrtcgrabacionesold;
