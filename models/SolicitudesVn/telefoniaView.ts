import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const telefoniaView = db.define('telefoniaView', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    numero: {
        type: DataTypes.BIGINT,
    },
    peticion: {
        type: DataTypes.JSON,
    },
    respuesta: {
        type: DataTypes.JSON,
    },
    idSolicitud: {
        type: DataTypes.BIGINT,
    },
    nombreProspecto: {
        type: DataTypes.STRING,
    },
    correo: {
        type: DataTypes.BIGINT,
    },
    sexo: {
        type: DataTypes.STRING,
    },
    edad: {
        type: DataTypes.INTEGER,
    },
    url: {
        type: DataTypes.STRING,
    },
});


export default telefoniaView;
