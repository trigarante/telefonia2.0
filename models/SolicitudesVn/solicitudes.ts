import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const solicitudes = db.define('solicitudes', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    idCotizacionAli: {
        type: DataTypes.BIGINT,
    },
    idEmpleado: {
        type: DataTypes.BIGINT,
    },
    idEstadoSolicitud: {
        type: DataTypes.INTEGER,
    },
    idEtiquetaSolicitud: {
        type: DataTypes.BIGINT,
    },
    idFlujoSolicitud: {
        type: DataTypes.BIGINT,
    },
    idFase: {
        type: DataTypes.STRING,
    },
    fechaSolicitud: {
        type: DataTypes.DATE,
    },
    comentarios: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    idSubetiquetaSolicitud: {
        type: DataTypes.INTEGER,
    },
});


export default solicitudes;
