import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const empleadoView = db.define('empleadoView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idBanco: {
        type: DataTypes.INTEGER,
    },
    idCandidato: {
        type: DataTypes.INTEGER,
    },
    idPuesto: {
        type: DataTypes.INTEGER,
    },
    idTipoPuesto: {
        type: DataTypes.INTEGER,
    },
    r: {
        type: DataTypes.INTEGER,
    },
    idUsuario: {
        type: DataTypes.INTEGER,
    },
    puestoDetalle: {
        type: DataTypes.STRING,
    },
    tipo: {
        type: DataTypes.STRING,
    },
    imagenEmpleado: {
        type: DataTypes.STRING,
    },
    recontratables: {
        type: DataTypes.STRING,
    },
    fechaAltaIMSS: {
        type: DataTypes.DATE,
    },
    documentosPersonales: {
        type: DataTypes.STRING,
    },
    documentosAdministrativos: {
        type: DataTypes.STRING,
    },
    fechaIngreso: {
        type: DataTypes.DATE,
    },
    sueldoDiario: {
        type: DataTypes.INTEGER,
    },
    sueldoMensual: {
        type: DataTypes.INTEGER,
    },
    kpi: {
        type: DataTypes.INTEGER,
    },
    fechaCambioSueldo: {
        type: DataTypes.DATE,
    },
    ctaClabe: {
        type: DataTypes.STRING,
    },
    fechaAsignacion: {
        type: DataTypes.DATE,
    },
    kpiMensual: {
        type: DataTypes.INTEGER,
    },
    kpiTrimestral: {
        type: DataTypes.INTEGER,
    },
    kpiSemestral: {
        type: DataTypes.INTEGER,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    comentarios: {
        type: DataTypes.STRING,
    },
    banco: {
        type: DataTypes.STRING,
    },
    idArea: {
        type: DataTypes.INTEGER,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    idSede: {
        type: DataTypes.INTEGER,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    area: {
        type: DataTypes.STRING,
    },
    idEmpresa: {
        type: DataTypes.INTEGER,
    },
    sede: {
        type: DataTypes.STRING,
    },
    idGrupo: {
        type: DataTypes.INTEGER,
    },
    empresa: {
        type: DataTypes.STRING,
    },
    grupo: {
        type: DataTypes.STRING,
    },
    puesto: {
        type: DataTypes.STRING,
    },
    puestoTipo: {
        type: DataTypes.STRING,
    },
    idPrecandidato: {
        type: DataTypes.INTEGER,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    apellidoPaterno: {
        type: DataTypes.STRING,
    },
    apellidoMaterno: {
        type: DataTypes.STRING,
    },
    fechaNacimiento: {
        type: DataTypes.DATE,
    },
    email: {
        type: DataTypes.STRING,
    },
    genero: {
        type: DataTypes.STRING,
    },
    idestadoCivil: {
        type: DataTypes.INTEGER,
    },
    idEscolaridad: {
        type: DataTypes.INTEGER,
    },
    idEstadoEscolaridad: {
        type: DataTypes.INTEGER,
    },
    cp: {
        type: DataTypes.STRING,
    },
    idColonia: {
        type: DataTypes.INTEGER,
    },
    colonia: {
        type: DataTypes.STRING,
    },
    calle: {
        type: DataTypes.STRING,
    },
    numeroExterior: {
        type: DataTypes.STRING,
    },
    numeroInterior: {
        type: DataTypes.STRING,
    },
    telefonoFijo: {
        type: DataTypes.STRING,
    },
    telefonoMovil: {
        type: DataTypes.STRING,
    },
    idEstadoRH: {
        type: DataTypes.INTEGER,
    },
    fechaCreacion: {
        type: DataTypes.DATE,
    },
    fechaBaja: {
        type: DataTypes.DATE,
    },
    tiempoTraslado: {
        type: DataTypes.INTEGER,
    },
    idMedioTraslado: {
        type: DataTypes.INTEGER,
    },
    idEstacion: {
        type: DataTypes.INTEGER,
    },
    estadoCivil: {
        type: DataTypes.INTEGER,
    },
    escolaridad: {
        type: DataTypes.STRING,
    },
    estadoEscolar: {
        type: DataTypes.STRING,
    },
    estadoRH: {
        type: DataTypes.STRING,
    },
    medio: {
        type: DataTypes.STRING,
    },
    estacion: {
        type: DataTypes.STRING,
    },
    lineas: {
        type: DataTypes.STRING,
    },
    idEtapa: {
        type: DataTypes.INTEGER,
    },
    curp: {
        type: DataTypes.STRING,
    },
    recontratable: {
        type: DataTypes.STRING,
    },
    detalleBaja: {
        type: DataTypes.STRING,
    },
    fechaRegistroBaja: {
        type: DataTypes.DATE,
    },
    nombreEtapa: {
        type: DataTypes.STRING,
    },
    imss: {
        type: DataTypes.STRING,
    },
    rfc: {
        type: DataTypes.STRING,
    },
    idRazonSocial: {
        type: DataTypes.INTEGER,
    },
    idEstadoFiniquito: {
        type: DataTypes.INTEGER,
    },
    fechaFiniquito: {
        type: DataTypes.DATE,
    },
    montoFiniquito: {
        type: DataTypes.INTEGER,
    },
    usuario: {
        type: DataTypes.STRING,
    },
    idTurnoEmpleado: {
        type: DataTypes.INTEGER,
    },
    turno: {
        type: DataTypes.STRING,
    },
    horario: {
        type: DataTypes.STRING,
    },
    tarjetaSecundaria: {
        type: DataTypes.STRING,
    },
    idTipoSubarea: {
        type: DataTypes.INTEGER,
    },
    idEstadoSolicitudBaja: {
        type: DataTypes.INTEGER,
    },
});

export default empleadoView;
