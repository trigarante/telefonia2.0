import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const empleadosLlamadaInView = db.define('empleadosEnLlamadaInView', {
    idEmpleado: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    estadoLlamada: {
        type: DataTypes.STRING,
    },
    nombreEjecutivo: {
        type: DataTypes.STRING,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
    tiempoLlamada: {
        type: DataTypes.INTEGER,
    },
});

export default empleadosLlamadaInView;
