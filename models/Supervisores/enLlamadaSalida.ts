import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const enLlamadaSalida = db.define('enLlamadaSalidaPjsip', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    idAsterisk: {
        type: DataTypes.STRING
    },
    subarea: {
        type: DataTypes.STRING
    }
});

export default enLlamadaSalida;
