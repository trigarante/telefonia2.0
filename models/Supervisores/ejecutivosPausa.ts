import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const sessionUsuariosPausaView = db.define('sessionUsuariosPausaView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    estadoSesion: {
        type: DataTypes.STRING,
    },
    fechaInicioSesion: {
        type: DataTypes.DATE,
    },
    fechaFinSesion: {
        type: DataTypes.DATE,
    },
    motivosPausa: {
        type: DataTypes.STRING,
    },
    tiempoPausa: {
        type: DataTypes.INTEGER,
    },
    fechaInicioEstadoUsuario: {
        type: DataTypes.DATE,
    },
    fechaFinalEstadoUsuario: {
        type: DataTypes.DATE,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    extension: {
        type: DataTypes.STRING,
    },
});

export default sessionUsuariosPausaView;
