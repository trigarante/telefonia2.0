import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const enLlamadaEntrante = db.define('enLlamadaInPjsip', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    subarea: {
        type: DataTypes.STRING
    }
});

export default enLlamadaEntrante;
