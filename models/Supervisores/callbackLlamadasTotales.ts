import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const empleadoLlamadasSalidasView = db.define('empleadoLlamadasSalidasView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    apellidoPaterno: {
        type: DataTypes.STRING,
    },
    apellidoMaterno: {
        type: DataTypes.STRING,
    },
    idTipoLlamada: {
        type: DataTypes.INTEGER,
    },
    idEstadoLlamada: {
        type: DataTypes.INTEGER,
    },
    TotalLlamadas: {
        type: DataTypes.INTEGER,
    },
    porRealizar: {
        type: DataTypes.INTEGER,
    },
    realizadas: {
        type: DataTypes.INTEGER,
    },
});

export default empleadoLlamadasSalidasView;
