import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const usuariosLineaView = db.define('usuariosLineaView', {
    idSesion: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    idArea: {
        type: DataTypes.INTEGER,
    },
    estadoSesion: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
    segundosLinea: {
        type: DataTypes.INTEGER,
    },
});

export default usuariosLineaView;
