import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const subetiquetaSolicitud = db.define('subetiquetaSolicitud' , {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idEtiquetaSolicitud: {
        type: DataTypes.TINYINT,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.TINYINT,
    },
});
 export default subetiquetaSolicitud;
