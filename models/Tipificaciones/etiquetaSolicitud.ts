import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const etiquetaSolicitud = db.define('etiquetaSolicitud' , {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    idEstadoSolicitud: {
        type: DataTypes.INTEGER,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.TINYINT,
    },
});

export default etiquetaSolicitud;
