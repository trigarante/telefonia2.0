import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const estadoSolicitud = db.define('estadoSolicitud' , {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    estado: {
        type: DataTypes.STRING,
    },
    activo: {
        type: DataTypes.TINYINT,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
    idFlujoTipificacion: {
        type: DataTypes.INTEGER,
    },
});

export default estadoSolicitud;
