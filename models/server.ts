import express, {Application} from 'express';
import telefoniaRoutes from '../routes/telefonia';
import callbackRoutes from '../routes/callback';
import pjsipRoutes from '../routes/pjsipRoutes/pjsipRouter';
import cors from 'cors';

import {db, dbpjsip} from '../db/connection';


class Server {

    private app: Application;
    private port: string;

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '8080';

        // Métodos iniciales
        this.dbConnection();
        this.pjsipConnection();
        this.middlewares();
        this.routes();
    }

    async dbConnection() {

        try {
            await db.authenticate();
            console.log('Database online');
        } catch (error) {
            throw new Error(error);
        }

    }
    async pjsipConnection() {
        try {
            await dbpjsip.authenticate();
            console.log('Base Pjsip Online');
        } catch (e) {
            throw new Error(e);
        }
    }

    middlewares() {

        // CORS
        this.app.use(cors());

        // Lectura del body
        this.app.use(express.json());

        // Carpeta pública
        this.app.use(express.static('public'));
    }


    routes() {
        this.app.use('/api', telefoniaRoutes);
        this.app.use('/callcenter', callbackRoutes);
        this.app.use('/pjsip', pjsipRoutes);
    }


    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port);
        })
    }

}

export default Server;
