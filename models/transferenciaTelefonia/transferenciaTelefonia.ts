import { DataTypes} from "sequelize";
import {db} from '../../db/connection';
const transferenciaTelefonia = db.define('transferenciaTelefonia', {
   id: {
       type: DataTypes.INTEGER,
       primaryKey: true,
       autoIncrement: true,
   },
    nombreCampania: {
       type: DataTypes.STRING,
    },
    numeroTelefono: {
       type: DataTypes.STRING
    }
});

export default transferenciaTelefonia;
