import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const numerosRegistro = db.define('numerosRegistro', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idRegistro: {
        type: DataTypes.BIGINT
    },
    idSolicitud: {
        type: DataTypes.BIGINT
    },
    numero: {
        type: DataTypes.STRING
    }
});

export default numerosRegistro;
