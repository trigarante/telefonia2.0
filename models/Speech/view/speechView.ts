import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';
const speechView = db.define('speechView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    speech: {
        type: DataTypes.STRING
    },
    idEstadoSpeech: {
        type: DataTypes.INTEGER
    },
    descripcion: {
        type: DataTypes.STRING
    },
    nombreEmpleadoCreador: {
        type: DataTypes.STRING
    },
    appEmpleadoCreador: {
        type: DataTypes.STRING
    },
    apmEmpleadoCreador: {
        type: DataTypes.STRING
    },
    nombreEmpleadoAutorizo: {
        type: DataTypes.STRING
    },
    appEmpleadoAutorizo: {
        type: DataTypes.STRING
    },
    apmEmpleadoAutorizo: {
        type: DataTypes.STRING
    },
    titulo: {
        type: DataTypes.STRING
    },
    idEmpresa: {
        type: DataTypes.INTEGER
    },
    idGrupo: {
        type: DataTypes.INTEGER
    },
    idArea: {
        type: DataTypes.INTEGER
    },
    idSede: {
        type: DataTypes.INTEGER
    },
    idSubArea: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaAutorizacion: {
        type: DataTypes.DATE
    },
    fechaInhabilitacion: {
        type: DataTypes.DATE
    },
    fechaDesautorizacion: {
        type: DataTypes.DATE
    },
});

export default speechView;
