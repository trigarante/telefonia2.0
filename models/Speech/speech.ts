import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const speech = db.define('speech', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idSubArea: {
        type: DataTypes.INTEGER
    },
    speech: {
        type: DataTypes.STRING
    },
    idEstadoSpeech: {
        type: DataTypes.INTEGER
    },
    idEmpleadoCreador: {
        type: DataTypes.INTEGER
    },
    idEmpleadoAtorizo: {
        type: DataTypes.INTEGER
    },
    titulo: {
        type: DataTypes.STRING
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaAutorizacion: {
        type: DataTypes.DATE
    },
    fechaInhabilitacion: {
        type: DataTypes.DATE
    },
    fechaDesautorizacion: {
        type: DataTypes.DATE
    }
});

export default speech;
