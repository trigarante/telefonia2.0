import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const CurrentServer = db.define('actualServidorTelefonia', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    currentServer: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.TINYINT
    }
});

export default CurrentServer;
