import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const motivosPausa = db.define('motivosPausa', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    descripcion: {
        type: DataTypes.INTEGER,
    },
    activo: {
        type: DataTypes.TINYINT,
    },
    tiempo: {
        type: DataTypes.STRING,
    },
    idTipoPausa: {
      type: DataTypes.INTEGER,
    },
});

export default motivosPausa;
