import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const estadoSesion = db.define('estadoSesion', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    descripcion: {
        type: DataTypes.INTEGER,
    },
    activo: {
        type: DataTypes.TINYINT,
    },
});

export default estadoSesion;
