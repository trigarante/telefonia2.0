import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const llamadaEntrantes = db.define('llamadaEntrantes' ,{
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idTipoLlamada: {
        type: DataTypes.INTEGER,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaFinal: {
        type: DataTypes.DATE,
    },
    idEstadoLlamada: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    idSubEtiqueta: {
        type: DataTypes.INTEGER,
    },
    idSolicitud: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.STRING,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    idAsteriskTroncal: {
        type: DataTypes.STRING,
    },
    finTipificacion: {
        type: DataTypes.DATE,
    },
    idContizacionesAli: {
        type: DataTypes.INTEGER,
    },
});

export default llamadaEntrantes;
