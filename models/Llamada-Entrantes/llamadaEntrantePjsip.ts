import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const LlamadaEntrantes = db.define('llamadaEntrantesPjsip' ,{
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaFinal: {
        type: DataTypes.DATE,
    },
    idEstadoLlamada: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    idSubEtiqueta: {
        type: DataTypes.INTEGER,
    },
    idSolicitud: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.STRING,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    finTipificacion: {
        type: DataTypes.DATE,
    },
    idTipoServer: {
        type: DataTypes.TINYINT,
    },
});

export default LlamadaEntrantes;
