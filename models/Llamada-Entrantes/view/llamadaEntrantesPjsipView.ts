import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';

const LlamadaEntrantesPjsipView = db.define('llamadaEntrantesPjsipView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING
    },
    idSubarea: {
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING
    },
    descripcion: {
        type: DataTypes.STRING
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    idTipoServer: {
        type: DataTypes.TINYINT,
    },
});

export default LlamadaEntrantesPjsipView;
