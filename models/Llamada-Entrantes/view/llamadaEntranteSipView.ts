import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';

const LlamadaEntrantesSipView = db.define('llamadaEntrantesSipView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING
    },
    idSubarea: {
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING
    },
    descripcion: {
        type: DataTypes.STRING
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    fechaRegistro: {
        type: DataTypes.DATE
    }
});

export default LlamadaEntrantesSipView;
