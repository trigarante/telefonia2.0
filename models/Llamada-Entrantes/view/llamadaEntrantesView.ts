import { DataTypes } from 'sequelize';
import {db} from '../../../db/connection';

const LlamadaEntrantesView = db.define('llamadaEntrantesView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idTipoLlamada: {
        type: DataTypes.INTEGER,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaFinal: {
        type: DataTypes.DATE,
    },
    idEstadoLlamada: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    idAsteriskUno: {
        type: DataTypes.STRING,
    },
    idSubEtiqueta: {
        type: DataTypes.INTEGER,
    },
    idSolicitud: {
        type: DataTypes.INTEGER,
    },
    numero: {
        type: DataTypes.INTEGER,
    },
    fechaRegistro: {
        type: DataTypes.DATE,
    },
    estadoLlamada: {
        type: DataTypes.STRING,
    },
    idEmpleadoSolicitud: {
        type: DataTypes.INTEGER,
    },
    nombreProspecto: {
        type: DataTypes.STRING,
    },
    segundosLlamada: {
        type: DataTypes.INTEGER,
    },
    nombreArchivo: {
        type: DataTypes.STRING,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    idSolicitudes: {
        type: DataTypes.INTEGER,
    },
    segundosTipificacion: {
        type: DataTypes.INTEGER,
    },
    subEtiquetaSolicitud: {
        type: DataTypes.STRING,
    },
    extension: {
        type: DataTypes.STRING,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    idSubarea: {
        type: DataTypes.INTEGER,
    },
});

export default LlamadaEntrantesView;
