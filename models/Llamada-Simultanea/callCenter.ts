import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const callCenter = db.define('callCenter', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    aseguradora: {
      type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING
    },
    ejecutivo: {
        type: DataTypes.TINYINT
    }
});

export default callCenter;
