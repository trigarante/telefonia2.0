import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const aseguradorasGmm = db.define('aseguradorasGmm', {
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
    },
    aseguradora: {
        type: DataTypes.STRING,
    },
    agente: {
        type: DataTypes.STRING,
    },
    numero: {
        type: DataTypes.STRING,
    },
    ext: {
        type: DataTypes.STRING,
    }
});

export default aseguradorasGmm;
