import {DataTypes} from "sequelize";
import {db} from '../../db/connection';

const seguimientoLlamadaView = db.define('seguimientoLlamadaView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    eventtype: {
        type: DataTypes.STRING,
    },
    eventtime: {
        type: DataTypes.DATE,
    },
    userdeftype: {
        type: DataTypes.STRING,
    },
    cid_name: {
        type: DataTypes.STRING,
    },
    cid_num: {
        type: DataTypes.STRING,
    },
    cid_ani: {
        type: DataTypes.STRING,
    },
    cid_rdnis: {
        type: DataTypes.STRING,
    },
    cid_dnid: {
        type: DataTypes.STRING,
    },
    exten: {
        type: DataTypes.STRING,
    },
    context: {
        type: DataTypes.STRING,
    },
    channame: {
        type: DataTypes.STRING,
    },
    appname: {
        type: DataTypes.STRING,
    },
    appdata: {
        type: DataTypes.STRING,
    },
    amaflags: {
        type: DataTypes.INTEGER,
    },
    accountcode: {
        type: DataTypes.STRING,
    },
    peeraccount: {
        type: DataTypes.STRING,
    },
    uniqueid: {
        type: DataTypes.STRING,
    },
    linkedid: {
        type: DataTypes.STRING,
    },
    userfield: {
        type: DataTypes.STRING,
    },
    peer: {
        type: DataTypes.STRING,
    },
    tiempoEspera: {
        type: DataTypes.INTEGER,
    },
    aplicacion: {
        type: DataTypes.INTEGER,
    },
});

export default seguimientoLlamadaView;
