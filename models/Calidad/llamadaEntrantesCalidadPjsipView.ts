import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const llamadaEntrantesCalidadPjsipView = db.define('llamadaEntrantesCalidadPjsipView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    apellidoPaterno: {
        type: DataTypes.STRING,
    },
    apellidoMaterno: {
        type: DataTypes.STRING,
    },
    nombreArchivo: {
        type: DataTypes.STRING,
    },
    segundosLlamada: {
        type: DataTypes.INTEGER,
    },
    estadoRh: {
        type: DataTypes.STRING,
    },
    numeroCliente: {
        type: DataTypes.STRING,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
    idTipoServer: {
        type: DataTypes.TINYINT
    },
    subetiqueta: {
        type: DataTypes.STRING
    },
});

export default llamadaEntrantesCalidadPjsipView;
