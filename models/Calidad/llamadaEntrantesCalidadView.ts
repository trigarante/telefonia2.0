import { DataTypes } from 'sequelize';
import {db} from '../../db/connection';

const llamadaEntrantesCalidadView = db.define('llamadaEntrantesCalidadView', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    idEmpleado: {
        type: DataTypes.INTEGER,
    },
    nombreArchivo: {
        type: DataTypes.STRING,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    apellidoPaterno: {
        type: DataTypes.STRING,
    },
    apellidoMaterno: {
        type: DataTypes.STRING,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    segundosLlamada: {
        type: DataTypes.INTEGER,
    },
    estadoRh: {
        type: DataTypes.STRING,
    },
    numeroCliente: {
        type: DataTypes.STRING,
    },
    subarea: {
        type: DataTypes.STRING,
    },
    idAsterisk: {
        type: DataTypes.STRING,
    },
});

export default llamadaEntrantesCalidadView;
