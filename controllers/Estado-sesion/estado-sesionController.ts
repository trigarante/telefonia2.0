import { Request, Response } from 'express';
import estadoSesion from "../../models/Estado-Sesion/estado-sesion";
import {Op} from "sequelize";
import motivosPausa from "../../models/Estado-Sesion/motivos-pausa";

export const getEstados = async (req: Request, res: Response) => {
  try {
      const estados = await estadoSesion.findAll({
          where: {[Op.or]: [{activo: 1}, {activo: 2}]}
      });
      res.json(estados);
  }catch (e) {
      res.status(500).json(e);
  }
};

export const getMotivosPausaByIdTipoPausa = async (req: Request, res: Response) => {
    const {idTipoPausa} = req.params;
    try {
        const motivos = await motivosPausa.findAll({
            where: {idTipoPausa}
        });
        res.json(motivos);
    } catch (e) {
        res.status(500).json(e);
    }
};
