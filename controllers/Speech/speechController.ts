import { Request, Response } from 'express';
import speech from "../../models/Speech/speech";

export const getSpeech = async (req: Request, res: Response) => {
    const {idSubarea} = req.params;
  try{
      const dialogo = await speech.findAll({
          where: {idSubArea: idSubarea}
      });
      res.json(dialogo);
  }  catch (e) {
      res.status(500).json(e);
  }
};
