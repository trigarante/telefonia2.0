import { Request, Response } from 'express';
import aseguradorasGmm from "../../models/GMM/aseguradorasGmm";

export const getAseguradorasGmm = async (req: Request, res: Response) => {
  try {
      const aseguradoras = await aseguradorasGmm.findAll();
      res.json(aseguradoras);
  } catch (e) {
      res.status(500).json(e);
  }
};
