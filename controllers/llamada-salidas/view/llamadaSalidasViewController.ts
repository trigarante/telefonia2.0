import { Request, Response } from 'express';
import llamadaSalidasView from "../../../models/Llamada-Salidas/view/llamadaSalidasView";
import LlamadaSalidasPjsipView from "../../../models/Llamada-Salidas/view/llamadaSalidasPjsipView";
export const getLlamadasByIdSolicitud = async (req: Request, res: Response) => {
    const {idSolicitud} = req.params;
    try {
        const llamadas = await llamadaSalidasView.findAll({
            where: {idSolicitud}
        });
        res.json(llamadas);
    }  catch (e) {
        res.status(500).json(e);
    }
};

export const getLlamadasByIdRegistro = async (req: Request, res: Response) => {
 const {idRegistro} = req.params;
 try {
     const llamadas = await llamadaSalidasView.findAll({
        where: {idRegistro}
     });
     res.json(llamadas);
 } catch (e) {
     res.status(500).json(e);
 }
};

export const getLlamadasPorSolicitudPjsip = async (req: Request, res: Response) => {
  try {
      const {idSolicitud} = req.params;
      const llamadasOutPjsip = await LlamadaSalidasPjsipView.findAll({
         where: {
             idSolicitud,
         },
          order: [
              ['fechaInicio', 'DESC']
          ]
      });
      res.json(llamadasOutPjsip);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const llamadasporIdRegistroPjsip = async (req: Request, res: Response) => {
    const {idRegistro} = req.params;
  try{
      const llamadasOutPjsip = await LlamadaSalidasPjsipView.findAll({
          where: {
              idRegistro,
          },
          order: [
              ['fechaInicio', 'DESC']
          ]
      });
      res.json(llamadasOutPjsip);
  }catch (e) {
      res.status(500).json(e);
  }
};
