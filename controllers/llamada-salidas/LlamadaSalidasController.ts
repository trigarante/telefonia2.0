import { Request, Response } from 'express';
import solicitudes from "../../models/SolicitudesVn/solicitudes";
import cdrPjsip from "../pjsip/cdr/cdr";
import LlamadaSalidasPjsip from "../../models/Llamada-Salidas/LlamadaSalidasPjsip";
import {Op} from "sequelize";
import moment from "moment-timezone";
import logwebrtcgrabaciones from "../../models/Grabaciones/logwebrtcgrabaciones";

export const inicioLlamada = async (req: Request, res: Response) => {
    const { body } = req;
    try {
        const llamadaSinTerminar: any = await LlamadaSalidasPjsip.findAll({
           where: {
               idEstadoLlamada: 4,
               idEmpleado: body.idEmpleado,
           },
        });
        if (llamadaSinTerminar.length > 0) {
            llamadaSinTerminar.forEach((llamada: any) => {
                 llamada.update({
                   idEstadoLlamada: 6,
                });
            });
        }
        const nuevaLllamada = await LlamadaSalidasPjsip.create({
            idEmpleado: body.idEmpleado,
            numero: body.numero,
            idTipoLlamada: body.idTipoLlamada,
            idSolicitud: body.idSolicitud,
            fechaInicio: moment().tz('America/Mexico_City').format(),
            fechaRegistro: moment().tz('America/Mexico_City').format(),
            idRegistro: body.idRegistro,
            idEstadoLlamada: 4
        });
        res.json(nuevaLllamada);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const setAsteriskId = async(req: Request, res: Response) => {
    const {numero, idLlamada} = req.params;
  try {
      const logwebrtc: any = await logwebrtcgrabaciones.findOne({
          where: {src: numero},
          order: [
              ['id', 'DESC']
          ]
      });
      const llamada = await LlamadaSalidasPjsip.findByPk(idLlamada);
      if (!llamada) {
          return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
      }
      await llamada.update({
          idAsterisk: logwebrtc.uniqueid,
      });
      res.json(logwebrtc.uniqueid);
  } catch (e) {
      console.log(e);
      res.status(500).json(e);
  }
};

export const finLlamada = async (req: Request, res: Response) => {
    const { idLlamada }   = req.params;
    const { body } = req;
    try {
        const llamada = await LlamadaSalidasPjsip.findByPk(idLlamada);
        if (!llamada) {
            return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
        }
        await llamada.update({
            idEstadoLlamada: body.idEstadoLlamada || 3,
            fechaFinal: moment().tz('America/Mexico_City').format()
        });
        res.json(llamada);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const guardarSubetiqueta = async (req: Request, res: Response) => {
    const { idLlamada }   = req.params;
    const { body } = req;
    try {
        const llamada = await LlamadaSalidasPjsip.findByPk(idLlamada);
        if (!llamada) {
            return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
        }
        await llamada.update({
            idSubEtiqueta: body.idSubEtiqueta,
            comentarios: body.comentarios,
            finTipificacion: moment().tz('America/Mexico_City').format()
        });
        res.json(llamada);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const modificarSolicitud = async (req: Request, res: Response) => {
    const { idSolicitud }   = req.params;
    const { body } = req;
    try {
        const solicitud: any = await solicitudes.findByPk(idSolicitud);
        if (!solicitud) {
            return res.json(`No se encontro una solicitud con el id: ${idSolicitud}`);
        }
        await solicitud.update({
            idEstadoSolicitud: body.idEstadoSolicitud ? body.idEstadoSolicitud: solicitud.idEstadoSolicitud,
            idEtiquetaSolicitud: body.idEtiquetaSolicitud ? body.idEtiquetaSolicitud: solicitud.idEtiquetaSolicitud,
            idSubetiquetaSolicitud: body.idSubetiquetaSolicitud ? body.idSubetiquetaSolicitud: solicitud.idSubetiquetaSolicitud,
            comentarios: body.comentarios ? body.comentarios: solicitud.comentarios,
        });
        res.json(solicitud);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const postCallback = async(req: Request, res: Response) => {
  const { numero, idEmpleado, idSolicitud } = req.body;
  try {
      if (!numero || !idSolicitud || idEmpleado === 0 || numero === '0') {
          return res.status(500).json({
              msg: 'Datos invalidos',
              numero,
              idSolicitud,
              idEmpleado
          });
      }
      const llamadaCallback = await LlamadaSalidasPjsip.create({
          idTipoLlamada: 2,
          idEstadoLlamada: 2,
          idEmpleado,
          idSolicitud,
          numero
      });
      res.json({
          msg: 'Llamada callback registrada',
          llamadaCallback
      })
  } catch (e) {
      res.status(500).json(e);
  }
};

export const postCbPeru = async (req: Request, res: Response) => {
    const { numero, idEmpleado, idSolicitud } = req.body;
    try {
        if (!numero || !idSolicitud || idEmpleado === 0 || numero === '0') {
            return res.status(500).json({
                msg: 'Datos invalidos',
                numero,
                idSolicitud,
                idEmpleado
            });
        }
        const llamadaCallback = await LlamadaSalidasPjsip.create({
            idTipoLlamada: 11,
            idEstadoLlamada: 2,
            idEmpleado,
            idSolicitud,
            numero
        });
        res.json({
            msg: 'Llamada callback registrada',
            llamadaCallback
        })
    } catch (e) {
        res.status(500).json(e);
    }
};

export const inicioCallback = async (req: Request, res: Response) => {
    const {idLlamada} = req.params;
  try{
      const llamadaCallback = await LlamadaSalidasPjsip.findByPk(idLlamada);
      if (llamadaCallback) {
          await llamadaCallback.update({
              idEstadoLlamada: 4,
              fechaInicio: moment().tz('America/Mexico_City').format()
          });
          res.json(llamadaCallback);
      } else {
          res.status(404).json(`No existe llamada con id ${idLlamada}`);
      }
  }  catch (e) {
      res.status(500).json(e);
  }
};

export const endCallback = async (req: Request, res: Response) => {
    const {idLlamada} = req.params;
    const {idEstadoLlamada} = req.body;
    try {
        const llamadaCallback = await LlamadaSalidasPjsip.findByPk(idLlamada);
        if (llamadaCallback) {
            await llamadaCallback.update({
                idEstadoLlamada: idEstadoLlamada || 3,
                fechaFinal: moment().tz('America/Mexico_City').format()
            });
            res.json(llamadaCallback);
        } else {
            res.status(404).json(`No existe llamada con id ${idLlamada}`);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const getLlamadaCallback = async (req: Request, res: Response) => {
  const {idEmpleado} = req.params;
  try {
     const llamadasCallback = await LlamadaSalidasPjsip.findAll({
         where: {
             [Op.or] : [
                 {idTipoLlamada: 2, idEstadoLlamada: 2, idEmpleado},
                 {idTipoLlamada: 11, idEstadoLlamada: 2, idEmpleado},
             ]
         },
         order: [
             ['fechaRegistro', 'ASC']
         ]
     });
     res.json(llamadasCallback);
  }catch (e) {
      res.status(500).json(e);
  }
};

export const llamadaSinEtiqueta = async (req: Request, res: Response) => {
    const {idEmpleado} = req.params;
  try {
      // const llamadaSinCerrar = await LlamadaSalidasPjsip.findAll({
      //    where: {idEmpleado, fechaFinal: null, idEstadoLlamada: 4}
      // });
      // if (llamadaSinCerrar.length > 0) {
      //     llamadaSinCerrar.forEach((llamada) => {
      //            llamada.update({
      //            idEstadoLlamada: 6
      //        });
      //     });
      // }
      const llamadasSinEtiqueta = await LlamadaSalidasPjsip.findAll({
          where: {
              [Op.or]: [
                  {idEmpleado, idSubEtiqueta: null, idEstadoLlamada: 3 },
                  {idEmpleado, idSubEtiqueta: null, idEstadoLlamada: 5 }
              ]
          }
      });
      res.json(llamadasSinEtiqueta);
  }  catch (e) {
      res.status(500).json(e);
  }
};


export const findCallbackByIdSolicitud = async (req: Request, res: Response) => {
    const {idSolicitud, idEmpleadoViejo, idEmpleadoNuevo} = req.params;
  try {
      const llamadasCallback = await LlamadaSalidasPjsip.findAll({
          where: {idSolicitud, idTipoLlamada: 2, idEstadoLlamada: 2, idEmpleado: idEmpleadoViejo},
      });
      if (llamadasCallback.length > 0) {
          llamadasCallback.forEach((llamada: any) => {
             llamada.update({
                idEmpleado:  idEmpleadoNuevo
             });
          });
          res.json({msg: 'Llamadas Reasignadas', llamadasCallback});
      } else {
          res.json({msg: 'No se reasignaron llamadas', llamadasCallback});
      }
  }
   catch (e) {
      res.status(500).json(e);
  }
};


export const buscarSinTerminar = async (req: Request, res: Response) => {
    const {idAsterisk} = req.params;
  try {
      const llamadaTerminada = await cdrPjsip.findOne({
         where: {
             uniqueid: idAsterisk
         },
      });
      if(llamadaTerminada) {
          const llamadaSalida = await LlamadaSalidasPjsip.findOne({
              where: {idAsterisk, fechaFinal: null}
          });
          if (llamadaSalida) {
              await llamadaSalida.update({
                  idEstadoLlamada: 6,
              });
              res.json(1)
          } else {
              res.json(1);
          }
      } else {
          res.json(0);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};

export const llamadaSinidAsterik = async (req: Request, res: Response) => {
  const {idLlamada} = req.params;
  try{
      const llamadaSinIdAsterisk = await LlamadaSalidasPjsip.findByPk(idLlamada);
      if (llamadaSinIdAsterisk) {
          await llamadaSinIdAsterisk.update({
              idEstadoLlamada: 6
          });
          res.json(llamadaSinIdAsterisk);
      } else {
          res.json('fallo');
      }
  }catch (e) {
      res.status(500).json(e);
  }
};

export const sinTerminarByCDR = async (req: Request, res: Response) => {
    const {idAsterisk} = req.params;
  try {
      const llamadaTerminada = await cdrPjsip.findOne({
          where: {
              uniqueid: idAsterisk
          },
      });
      if (llamadaTerminada) {
          res.json(1);
      } else {
          res.json(0);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};




