import { Request, Response } from 'express';
import llamadaSalidasCalidadView from "../../models/Calidad/llamadaSalidasCalidadView";
import {Op} from "sequelize";
import llamadaEntrantesCalidadView from "../../models/Calidad/llamadaEntrantesCalidadView";
import llamadaSalidasCalidadPjsipView from "../../models/Calidad/llamadaSalidasCalidadPjsipView";
import llamadaEntrantesCalidadPjsipView from "../../models/Calidad/llamadaEntrantesCalidadPjsipView";

export const getLlamadasSalidaByDate = async (req: Request, res: Response) => {
    const fechaInicial = req.headers.fechainicio || 'fechaInicial';
    const fechaFin = req.headers.fechafinal || 'fechaFin';
  try {
      const [llamadasSip, llamadasPjsip] = await Promise.all([
          llamadaSalidasCalidadView.findAll({
              where: {
                  fechaInicio: {[Op.between]: [fechaInicial.toString(), fechaFin.toString()]},
              }
          }),
          llamadaSalidasCalidadPjsipView.findAll({
              where: {
                  fechaInicio: {[Op.between]: [fechaInicial.toString(), fechaFin.toString()]},
              }
          })
      ]);
      res.json({llamadasSip, llamadasPjsip});
  }catch (e) {
      res.status(500).json(e);
  }
};

export const getllamadasEntrantesByDate = async (req: Request, res: Response) => {
    const fechaInicial = req.headers.fechainicio || 'fechaInicial';
    const fechaFin = req.headers.fechafinal || 'fechaFin';
    try {
        const [llamadasSip, llamadasPjsip] = await Promise.all([
            llamadaEntrantesCalidadView.findAll({
               where: {
                   fechaInicio: {[Op.between]: [fechaInicial.toString(), fechaFin.toString()]},
               }
            }),
            llamadaEntrantesCalidadPjsipView.findAll({
                where: {
                    fechaInicio: {[Op.between]: [fechaInicial.toString(), fechaFin.toString()]},
                }
            })
        ]);
        res.json({llamadasSip, llamadasPjsip});
    }catch (e) {
        res.status(500).json(e);
    }
};
