import { Request, Response } from 'express';
import transferenciaTelefonia from "../../models/transferenciaTelefonia/transferenciaTelefonia";

export const getNumeros = async (req: Request, res: Response) => {
  try {
      const numeros = await transferenciaTelefonia.findAll();
      res.json(numeros);
  } catch (e) {
      res.status(500).json(e);
  }
};
