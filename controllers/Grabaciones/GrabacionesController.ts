import { Request, Response } from 'express';
import logwebrtcgrabaciones from "../../models/Grabaciones/logwebrtcgrabaciones";
import fs from "fs";
import readline from 'readline';
import {google} from 'googleapis';
import cdr from "../../models/Grabaciones/cdr";
import {Op} from "sequelize";
import LogwebrtcGrabaciones from "../../models/pjsip/logwebrtc/logwebrtcgrabaciones";
import cdrPjsip from "../pjsip/cdr/cdr";
import logwebrtcgrabacionesold from "../../models/Grabaciones/logwebrtcgrabacionesold";


export const getNombreArchivo = async (req: Request, res: Response) => {
    const { idAsterisk } = req.params;
  try {
      const logwebrtc = await logwebrtcgrabacionesold.findAll({
          where: {uniqueid: idAsterisk}
      });
      res.json(logwebrtc);
  }  catch (e) {
      res.status(500).json(e);
  }
};

export const getGrabacionOutbound = async (req: Request, resp: Response) => {
    const SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly', 'https://www.googleapis.com/auth/drive'];
    const TOKEN_PATH = 'token_telefonia.json';
    const archivo = req.header('nombreArchivo');

    fs.readFile('client_secret_telefonia.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        // Authorize a client with credentials, then call the Google Drive API.
        // @ts-ignore
        authorize(JSON.parse(content), listFiles);
    });

    const authorize = (credentials: any, callback: any) => {
        const {client_secret, client_id, redirect_uris} = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(
            client_id, client_secret, redirect_uris[0]);

        // Check if we have previously stored a token.
        fs.readFile(TOKEN_PATH, (err, token) => {
            if (err) return getAccessToken(oAuth2Client, callback);
            // @ts-ignore
            oAuth2Client.setCredentials(JSON.parse(token));
            callback(oAuth2Client);
        });
    };

    const getAccessToken = (oAuth2Client: any, callback: any) => {
        const authUrl = oAuth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
        });
        console.log('Authorize this app by visiting this url:', authUrl);
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
        rl.question('Enter the code from that page here: ', (code) => {
            rl.close();
            oAuth2Client.getToken(code, (err: any, token: any) => {
                if (err) return console.error('Error retrieving access token', err);
                oAuth2Client.setCredentials(token);
                // Store the token to disk for later program executions
                // tslint:disable-next-line:no-shadowed-variable
                fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                    if (err) return console.error(err);
                    console.log('Token stored to', TOKEN_PATH);
                });
                callback(oAuth2Client);
            });
        });
    };

    const listFiles = (auth: any) => {
        const drive = google.drive({version: 'v3', auth});
        // Se busca por mp3
        drive.files.list({
            q: `name = '${archivo}.mp3'`,
            fields: 'nextPageToken, files(webContentLink)',
        }, (err, res) => {
            if (err) {
                console.log('The API returned an error: ' + err);
                return resp.status(500).json({
                    msg: 'Error en la api de google',
                    err
                });
            }
            if (res) {
                const files: any = res.data.files;
                if (files.length > 0) {
                    resp.json(files);
                } else {
                    // se busca por wav
                    drive.files.list({
                        q: `name = '${archivo}.wav'`,
                        fields: 'nextPageToken, files(webContentLink)',
                    }, (er,ans) => {
                        if (er) {
                            console.log('The API returned an error: ' + er);
                            return resp.status(500).json({
                                msg: 'Error en la api de google',
                                er
                            });
                        }
                        if (ans) {
                            const recordings: any = ans.data.files;
                            resp.json(recordings);
                        }
                    });
                }
            }
        });
    }
};
export const buzonGrabacion = async (req: Request, res: Response) => {
    const {oldIdAsterisk, numero} = req.params;
    try {
        const originalCall: any = await cdr.findOne({
            where: {uniqueid: oldIdAsterisk}
        });
        if (originalCall) {
            const dateCall: any = new Date(originalCall.calldate);
            const fecha1 = new Date(dateCall.getTime() + 6000);
            const fecha2 = new Date(dateCall.getTime() - 6000);
            const buzonCall: any = await cdr.findOne({
                where: {calldate: {[Op.between]: [fecha2, fecha1]}, dst: numero}
            });
            if (buzonCall) {
                res.json(buzonCall.uniqueid);
            } else {
                return res.status(404).json('No se encontro grabacion');
            }
        } else {
            return res.status(404).json('No se encontro grabacion');
        }

    } catch (e) {
        console.log(e);
        res.status(500).json('error');
    }
};

export const getArchivoPjsip = async(req: Request, res: Response) => {
    try {
        const { idAsterisk } = req.params;
        try {
            const logwebrtc = await logwebrtcgrabaciones.findAll({
                where: {uniqueid: idAsterisk}
            });
            res.json(logwebrtc);
        }  catch (e) {
            res.status(500).json(e);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const buzonGrabacionPjsip = async(req: Request, res: Response) => {
    try {
        const {oldIdAsterisk, numero} = req.params;
        try {
            const originalCall: any = await cdrPjsip.findOne({
                where: {uniqueid: oldIdAsterisk}
            });
            if (originalCall) {
                const dateCall: any = new Date(originalCall.calldate);
                const fecha1 = new Date(dateCall.getTime() + 6000);
                const fecha2 = new Date(dateCall.getTime() - 6000);
                const buzonCall: any = await cdrPjsip.findOne({
                    where: {calldate: {[Op.between]: [fecha2, fecha1]}, dst: numero}
                });
                if (buzonCall) {
                    res.json(buzonCall.uniqueid);
                } else {
                    return res.status(404).json('No se encontro grabacion');
                }
            } else {
                return res.status(404).json('No se encontro grabacion');
            }

        } catch (e) {
            console.log(e);
            res.status(500).json('error');
        }
    } catch (e) {
        res.status(500).json(e);
    }
};
