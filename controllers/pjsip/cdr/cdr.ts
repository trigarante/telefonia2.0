import { DataTypes } from 'sequelize';
import  {dbpjsip} from "../../../db/connection";

const cdrPjsip = dbpjsip.define('cdr', {
    calldate: {
        type: DataTypes.DATE,
        primaryKey: true,
    },
    clid: {
        type: DataTypes.STRING,
    },
    src: {
        type: DataTypes.STRING,
    },
    dst: {
        type: DataTypes.STRING,
    },
    dcontext: {
        type: DataTypes.STRING,
    },
    channel: {
        type: DataTypes.STRING,
    },
    dstchannel: {
        type: DataTypes.STRING,
    },
    lastapp: {
        type: DataTypes.STRING,
    },
    lastdata: {
        type: DataTypes.STRING,
    },
    duration: {
        type: DataTypes.INTEGER,
    },
    billsec: {
        type: DataTypes.INTEGER,
    },
    disposition: {
        type: DataTypes.STRING,
    },
    amaflags: {
        type: DataTypes.INTEGER,
    },
    accountcode: {
        type: DataTypes.STRING,
    },
    uniqueid: {
        type: DataTypes.STRING,
    },
    userfield: {
        type: DataTypes.STRING,
    },
    did: {
        type: DataTypes.STRING,
    },
    recordingfile: {
        type: DataTypes.STRING,
    },
});

export default cdrPjsip;
