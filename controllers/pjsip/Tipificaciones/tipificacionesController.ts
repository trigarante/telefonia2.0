import {Request, Response} from "express";
import EstadoTipificacion from "../../../models/pjsip/Tipificaciones/estadoTipificacionModel";
import EtiquetaLlamada from "../../../models/pjsip/Tipificaciones/etiquetaLlamada";
import SubEtiquetaLlamada from "../../../models/pjsip/Tipificaciones/subEtiquetaLlamadaModel";
import LlamadaEntrantes from "../../../models/pjsip/llamada-entrantes/llamadaEntrnates";
import LlamadaSalidas from "../../../models/pjsip/llamada-salidas/llamadaSalidas";

export const getEstados = async (req: Request, res: Response) => {
  try{
      const estados = await EstadoTipificacion.findAll({
         where: {
             activo: 1
         }
      });
      res.json(estados);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getEtiquetas = async (req: Request, res: Response) => {
    const {idEstadoTipificacion} = req.params;
  try{
      const etiquetas = await EtiquetaLlamada.findAll({
         where: {
             idEstadoTipificacion,
             activo: 1,
         }
      });
      res.json(etiquetas);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getSubEtiquetas = async (req: Request, res: Response) => {
    const {idEtiquetaLlamada} = req.params;
    try {
        const subetiquetas = await SubEtiquetaLlamada.findAll({
            where: {
                idEtiquetaLlamada,
                activo: 1,
            }
        });
        res.json(subetiquetas);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const sinTipificarIn = async(req: Request, res: Response) => {
    const {idEmpleado} = req.params;
  try {
      const llamadaInSinTipificar: any = await LlamadaEntrantes.findOne({
         where: {
             idEmpleado,
             idSubEtiqueta: null
         },
          order: [
              ['id', 'DESC']
          ]
      });
      if (llamadaInSinTipificar) {
          res.json({
              idLlamada: llamadaInSinTipificar.id || 0,
              tipoLlamada: 'ib'
          });
      } else {
          res.json(llamadaInSinTipificar);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};

export const sinTipificarOut = async(req: Request, res: Response) => {
    const {idEmpleado} = req.params;
    const llamadaOutSinTipificar: any = await LlamadaSalidas.findOne({
        where: {
            idEmpleado,
            idSubEtiqueta: null
        },
        order: [
            ['id', 'DESC']
        ]
    });
    if (llamadaOutSinTipificar) {
        res.json({
            idLlamada: llamadaOutSinTipificar.id || 0,
            tipoLlamada: 'out',
        });
    } else {
        res.json(llamadaOutSinTipificar);
    }
};
