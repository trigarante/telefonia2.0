import {Request, Response} from "express";
import NumerosAdicionales from "../../../models/pjsip/llamada-salidas/numerosAdicionales";


export const agregarNumero = async (req: Request, res: Response) => {
    const {idSolicitud, numero} = req.body;
  try{
      const numeroNuevo = await NumerosAdicionales.create({
         idSolicitud,
         numero,
      });
      res.json(numeroNuevo);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getNumberByIdSolicitud = async (req: Request, res: Response) => {
    const {idSolicitud} = req.params;
  try {
      const numero = await NumerosAdicionales.findAll({
         where: {
             idSolicitud
         },
      });
      res.json(numero);
  } catch (e) {
      res.status(500).json(e);
  }
};
