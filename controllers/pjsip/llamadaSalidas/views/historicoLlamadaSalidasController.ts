import {Request, Response} from 'express';
import historicoLlamadaSalidas from "../../../../models/pjsip/llamada-salidas/views/historicoLlamadaSalidas";

export const getLlamadasByIdSOlicitud = async (req: Request, res: Response) => {
    const {idSolicitud} = req.params;
  try {
      const llamadas = await historicoLlamadaSalidas.findAll({
          where: {
              idSolicitud,
          },
          order: [
              ['id', 'DESC']
          ]
      });
      res.json(llamadas);
  } catch (e) {
      res.status(500).json(e);
  }
};
