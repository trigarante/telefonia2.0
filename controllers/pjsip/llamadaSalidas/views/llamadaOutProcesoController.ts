import {Request, Response} from "express";
import LlamadaOutProceso from "../../../../models/pjsip/llamada-salidas/views/llamadaOutProceso";
import usuariosSubarea from "../../../../models/Usuarios/usuariosSubarea";
import {Op} from "sequelize";


export const getLlamadasEnProceso = async(req: Request, res: Response) => {
    const { idUsuario, idPuesto, idTipo, idSubarea} = req.params;
  try {
      const usuarioSubarea: any = await usuariosSubarea.findAll({
          where: {idUsuario}
      });
      const subareas: any = [];
      if (idPuesto === '7' || (idPuesto === '8' && idTipo === '2')) {
          usuarioSubarea.forEach((users: any) => {
             subareas.push(users.idSubarea);
          });

          if (subareas.length === 0){
              subareas.push(Number(idSubarea));
          }
          const llamadasOut = await LlamadaOutProceso.findAll({
              where: {
                  idSubarea: {[Op.in]: subareas}
              },
              order: [
                  ['fechaInicio', 'ASC']
              ]
          });
          res.json(llamadasOut);
      } else {
          const llamadasOut = await LlamadaOutProceso.findAll();
          res.json(llamadasOut);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getLlamadaFinalizada = async (req: Request, res: Response) => {
  try {
      res.json('sin terminar');
  } catch (e) {
      res.status(500).json(e);
  }
};
