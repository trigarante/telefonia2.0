import {Request ,Response} from "express";
import LlamadaSalidas from "../../../models/pjsip/llamada-salidas/llamadaSalidas";
import moment from "moment-timezone";
import LogwebrtcGrabaciones from "../../../models/pjsip/logwebrtc/logwebrtcgrabaciones";
import cdrPjsip from "../cdr/cdr";
import cdr from "../../../models/Grabaciones/cdr";
import logwebrtcgrabaciones from "../../../models/Grabaciones/logwebrtcgrabaciones";
import llamadaSalidasGo from "../../../models/pjsip/llamada-salidas/llamadaSalidas";

export const inicioLlamada = async (req: Request, res: Response) => {
  try{
      const {idEmpleado, numero, idSolicitud, idRegistro} = req.body;
      const nuevaLlamada = await llamadaSalidasGo.create({
          idTipoLlamada: 36,
          idEstadoLlamada: 4,
          idEmpleado,
          numero,
          idSolicitud,
          idRegistro,
          fechaInicio: moment().tz('America/Mexico_City').format(),
      });
      res.json(nuevaLlamada);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const setAsteriskId = async (req: Request, res: Response) => {
  const {numero, idLlamada} = req.params;
  try {
      const log: any = await logwebrtcgrabaciones.findOne({
          where: {src: numero},
          order: [
              ['id', 'DESC']
          ]
      });
      const llamada = await llamadaSalidasGo.findByPk(idLlamada);
      if (!llamada) {
          return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
      }
      await llamada.update({
         idAsterisk: log.uniqueid,
      });
      res.json(log.uniqueid);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const finLlamada = async (req: Request, res: Response) => {
    const { idLlamada } = req.params;
    const { idEstadoLlamada } = req.body;
    try {
        const llamada = await llamadaSalidasGo.findByPk(idLlamada);
        if (!llamada) {
            return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
        }
        await llamada.update({
            idEstadoLlamada: idEstadoLlamada || 3,
            fechaFinal: moment().tz('America/Mexico_City').format(),
        });
        res.json(llamada);
    } catch (e) {
        res.status(500).json(e);
    }
};
export const finLlamadaIntermitencia = async (req: Request, res: Response) => {
    const { idLlamada } = req.params;
    try {
        const llamada: any = await llamadaSalidasGo.findByPk(idLlamada);
        if (llamada.idEstadoLlamada === 4) {
            await llamada.update({
               idEstadoLlamada: 6,
                fechaFinal: moment().tz('America/Mexico_City').format(),
            });
        }
        res.json(llamada);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const tipificarLlamada = async (req: Request, res: Response) => {
    const { idLlamada } = req.params;
    const { idSubEtiqueta } = req.body;
  try{
      const llamada = await llamadaSalidasGo.findByPk(idLlamada);
      if (!llamada) {
          return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
      }
      await llamada.update({
          idSubEtiqueta,
          finTipificacion: moment().tz('America/Mexico_City').format(),
      });
      res.json(llamada);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const buscarSinTerminar = async (req: Request, res: Response) => {
    const {idAsterisk} = req.params;
    try {
        const llamadaTerminada = await cdr.findOne({
           where: {
               uniqueid: idAsterisk
           }
        });
        if(llamadaTerminada) {
            const llamadaSalida = await llamadaSalidasGo.findOne({
                where: {idAsterisk}
            });
            if (llamadaSalida) {
                await llamadaSalida.update({
                    idEstadoLlamada: 6,
                });
                res.json(1)
            } else {
                res.json(1);
            }
        } else {
            res.json(0);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};
