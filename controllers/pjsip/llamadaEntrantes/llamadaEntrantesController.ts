import {Request ,Response} from "express";

import LogwebrtcGrabaciones from "../../../models/pjsip/logwebrtc/logwebrtcgrabaciones";
import moment from "moment-timezone";
import {Op} from "sequelize";
import llamadaEntrantesGo from "../../../models/pjsip/llamada-entrantes/llamadaEntrnates";
import logwebrtcgrabaciones from "../../../models/Grabaciones/logwebrtcgrabaciones";


export const inicioLlamadaEntrante = async (req: Request, res: Response) => {
    const {idEmpleado, numero} = req.body;
  try{
      const inicioLlamada = await llamadaEntrantesGo.create({
         idEstadoLlamada: 4,
          idTipoLlamada: 3,
         idEmpleado,
          numero,
          fechaInicio: moment().tz('America/Mexico_City').format()
      });
      res.json(inicioLlamada);
  } catch (e) {
      res.status(500).json(e);
  }
};


export const setIdAsteriskIb = async (req: Request, res: Response) => {
    const {numero, idLlamada} = req.params;
  try{
      const log: any = await logwebrtcgrabaciones.findOne({
          where: {
              recordingfile: {
                  [Op.like]: `%${numero}%`
              },
              src: {
                  [Op.like]: `%@${numero}%`
              }
          },
          order: [
              ['id', 'DESC']
          ]
      });
      const llamada = await llamadaEntrantesGo.findByPk(idLlamada);
      if (!llamada) {
          return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
      }
      await llamada.update({
          idAsterisk: log.uniqueid
      });
      res.json(log.uniqueid);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const finLlamadaIb = async(req: Request, res: Response) => {
    const {idLlamada} = req.params;
    const {idSolicitud, idEstadoLlamada} = req.body;
    try{
        const llamada = await llamadaEntrantesGo.findByPk(idLlamada);
        if(llamada) {
            await llamada.update({
                idEstadoLlamada: idEstadoLlamada || 3,
                idSolicitud,
                fechaFinal: moment().tz('America/Mexico_City').format(),
            });
            res.json(llamada);
        }
    }catch (e) {
        res.status(500).json(e);
    }
};

export const guardarSubetiquetaIb = async (req: Request, res: Response) => {
    const {idLlamada} = req.params;
    const {idSubEtiqueta} = req.body;
    try {
        const llamadaIb = await llamadaEntrantesGo.findByPk(idLlamada);
        if(llamadaIb) {
            await llamadaIb.update({
                idSubEtiqueta,
                finTipificacion: moment().tz('America/Mexico_City').format(),
            });
            res.json(llamadaIb);
        } else {
            res.status(404).json(`No existe llamada con el id: ${idLlamada}`);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};
