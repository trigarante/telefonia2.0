import {Request, Response} from "express";
import historicoLlamadaEntantes from "../../../../models/pjsip/llamada-entrantes/views/historicoLlamadaEntrantes";

export const getLlamadasByIdEmpleado = async (req: Request, res: Response) => {
    const {idEmpleado} = req.params;
  try {
      const llamadasIb = await historicoLlamadaEntantes.findAll({
         where: {
             idEmpleado
         },
          order: [
              ['id', 'DESC']
          ]
      });
      res.json(llamadasIb);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getAllLlamadasIb = async (req: Request, res: Response) => {
  try{
      const llamadasIb = await historicoLlamadaEntantes.findAll({
          order: [
              ['id', 'DESC']
          ]
      });
      res.json(llamadasIb);
  } catch(e) {
      res.status(500).json(e);
  }
};
