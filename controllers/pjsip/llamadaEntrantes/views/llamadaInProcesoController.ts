import {Request, Response} from "express";
import usuariosSubarea from "../../../../models/Usuarios/usuariosSubarea";
import LlamadaInProceso from "../../../../models/pjsip/llamada-entrantes/views/llamadaInProceso";
import {Op} from "sequelize";


export const getLlamadaInEnProceso = async(req: Request, res: Response) => {
    const { idUsuario, idPuesto, idTipo, idSubarea} = req.params;
  try {
      const usuarioSubarea: any = await usuariosSubarea.findAll({
          where: {idUsuario}
      });
      const subareas: any = [];
      if (idPuesto === '7' || (idPuesto === '8' && idTipo === '2')) {
          usuarioSubarea.forEach((users: any) => {
              subareas.push(users.idSubarea);
          });

          if (subareas.length === 0){
              subareas.push(Number(idSubarea));
          }
          const llamadasOut = await LlamadaInProceso.findAll({
              where: {
                  idSubarea: {[Op.in]: subareas}
              },
              order: [
                  ['fechaInicio', 'ASC']
              ]
          });
          res.json(llamadasOut);
      } else {
          const llamadasOut = await LlamadaInProceso.findAll();
          res.json(llamadasOut);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};
