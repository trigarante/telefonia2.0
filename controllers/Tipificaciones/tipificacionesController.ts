import { Request, Response } from 'express';
import estadoSolicitud from "../../models/Tipificaciones/estadoSolicitud";
import etiquetaSolicitud from "../../models/Tipificaciones/etiquetaSolicitud";
import subetiquetaSolicitud from "../../models/Tipificaciones/subetiquetaSolicitud";
import {Op} from "sequelize";

export const getEstadoSolicitud = async (req: Request, res: Response) => {
    try {
        const estados = await estadoSolicitud.findAll({
            where: {activo: 1}
        });
        res.json(estados);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const getEstadoSolicitudReno = async (req: Request, res: Response) => {
    try {
        const estados = await estadoSolicitud.findAll({
            where: {
                idFlujoTipificacion: 2,
                activo: 0
            }
        });
        res.json(estados);
    } catch (e) {
        res.status(500).json(e);
    }
};
export const getEstadoSolicitudGm = async (req: Request, res: Response) => {
    try {
        const estados = await estadoSolicitud.findAll({
            where: {
                idFlujoTipificacion: 3,
                activo: 0
            }
        });
        res.json(estados);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const getEstadoSolicitudCobranza = async(req: Request, res: Response) => {
  try {
      const estados = await estadoSolicitud.findAll({
          where: {
              idFlujoTipificacion: 4,
              activo: 0
          }
      });
      res.json(estados);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getEtiquetaSolicitud = async (req: Request, res: Response) => {
    const { idEstadoSolicitud }   = req.params;
  try {
      const etiquetas = await etiquetaSolicitud.findAll({
          where: {idEstadoSolicitud, activo: 1}
      });
      res.json(etiquetas);
  }  catch (e) {
      res.status(500).json(e);
  }
};

export const getSubEtiquetaSolicitud = async (req: Request, res: Response) => {
    const { idEtiqueta }   = req.params;
    try {
        const subetiquetas = await subetiquetaSolicitud.findAll({
            where: {idEtiquetaSolicitud: idEtiqueta, activo: 1}
        });
        res.json(subetiquetas);
    }  catch (e) {
        res.status(500).json(e);
    }
};

export const getEstadoSolicitudAgenda = async (req: Request, res: Response) => {
  try {
      const ids = [1, 2, 10];
      const estados = await estadoSolicitud.findAll({
         where: {
           id: {[Op.in]: ids}
         },
      });
      res.json(estados);
  } catch (e) {
      res.status(500).json(e);
  }
};
