import { Request, Response } from 'express';
import telefoniaView from "../../models/SolicitudesVn/telefoniaView";

export const getSolicitud = async (req: Request, res: Response) => {
  const {idSolicitud} = req.params;
  try {
     const solicitud = await telefoniaView.findOne({
         where: {idSolicitud}
     });
     res.json(solicitud);
  } catch (e) {
      res.status(500).json(e);
  }
};
