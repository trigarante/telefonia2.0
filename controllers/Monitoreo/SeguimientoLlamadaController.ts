import { Request, Response } from 'express';
import seguimientoLlamadaView from "../../models/Monitoreo-in/seguimientoLlamadaView";
import usuariosLineaView from "../../models/Supervisores/ejecutivosLinea";
import {Op, where} from "sequelize";
import empleadosLlamadaInView from "../../models/Supervisores/empleadosEnLlamadaInView";

export const getSegLlamada = async (req: Request, res: Response) => {
  try {
      const llamadasEnCola = await seguimientoLlamadaView.findAll();
      res.json(llamadasEnCola);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getSegLlamadaReno = async (req: Request, res: Response) => {
    try {
        const llamadasEnCola = await seguimientoLlamadaView.findAll();
        res.json(llamadasEnCola);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const ejecutivosLinea = async(req: Request, res: Response) => {
    try {
        const ejecutivosEnLinea = await usuariosLineaView.findAll({
           where:  {
               [Op.or]: [
                   {idArea:487},
                   {idArea:488},
                   {idArea:489}
               ]
           }
        });
        res.json(ejecutivosEnLinea);
    }catch (e) {
        res.status(500).json(e);
    }
};

export const ejecutivosLineaReno = async(req: Request, res: Response) => {
    try {
        const ejecutivosEnLinea = await usuariosLineaView.findAll({
            where:  {
                [Op.or]: [
                    {idArea:487},
                    {idArea:488},
                    {idArea:489}
                ]
            }
        });
        res.json(ejecutivosEnLinea);
    }catch (e) {
        res.status(500).json(e);
    }
};

export const empleadoEnLlamadaIn = async (req: Request, res: Response) => {
  try{
      const empleadoLlamada = await empleadosLlamadaInView.findAll({
          where: {
              tiempoLlamada: {[Op.lte]: 7200}
          }
      });
      res.json(empleadoLlamada);
  }catch (e) {
      res.status(500).json(e);
  }
};

export const empleadoEnLlamadaReno = async (req: Request, res: Response) => {
    try{
        const empleadoLlamada = await empleadosLlamadaInView.findAll({
            where: {
                tiempoLlamada: {[Op.lte]: 7200}
            }
        });
        res.json(empleadoLlamada);
    }catch (e) {
        res.status(500).json(e);
    }
};


