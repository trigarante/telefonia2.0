import { Request, Response } from 'express';
import aplicacionesView from '../../models/Finanzas/views/aplicacionesView';

export const aplicacionesById = async (req: Request, res: Response) => {
    try {
        const id = req.params.id;
        const aplicaciones = await aplicacionesView.findOne({
            where: {id}
        });
        res.json(aplicaciones);
    } catch (e) {
        res.status(500).send(e);
    }
};