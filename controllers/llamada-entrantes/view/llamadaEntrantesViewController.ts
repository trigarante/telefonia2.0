import { Request, Response } from 'express';
import llamadaEntrantesView from '../../../models/Llamada-Entrantes/view/llamadaEntrantesView';
import usuariosSubarea from "../../../models/Usuarios/usuariosSubarea";
import LlamadaEntrantesView from "../../../models/Llamada-Entrantes/view/llamadaEntrantesView";
import {Op} from "sequelize";
import LlamadaEntrantesSipView from "../../../models/Llamada-Entrantes/view/llamadaEntranteSipView";
import LlamadaEntrantesPjsipView from "../../../models/Llamada-Entrantes/view/llamadaEntrantesPjsipView";
export const getLlamadasByEMpleado = async (req: Request, res: Response) => {
  const {idEmpleado} = req.params;
  try {
      const llamadas = await llamadaEntrantesView.findAll({
          where: {idEmpleado},
      });
      res.json(llamadas);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getLlamadasIbBySubarea = async (req: Request, res: Response) => {
    const {idUsuario, idPuesto, idSubarea, idEmpleado} = req.params;
  try {
      if (idPuesto === '8') {
          const llamadas = await llamadaEntrantesView.findAll({
              where: {idEmpleado},
          });
          res.json(llamadas);
      } else {
          const usuarioSubarea: any = await usuariosSubarea.findAll({
             where: {idUsuario}
          });
          const subareas: any = [];
          usuarioSubarea.forEach((users: any) => {
             subareas.push(users.idSubarea);
          });
          if (subareas.length === 0) {
              subareas.push(Number(idSubarea));
          }
          const llamadasSubarea = await LlamadaEntrantesView.findAll({
             where: {
                 idSubarea: {[Op.in]: subareas}
             },
              order: [
                  ['fechaRegistro', 'DESC']
              ]
          });
          res.json(llamadasSubarea);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getLlamadasSipPorEmpleado = async (req: Request, res: Response) => {
    const {idUsuario, idPuesto, idSubarea, idEmpleado} = req.params;
    try {
        if (idPuesto === '8') {
            const llamadas = await LlamadaEntrantesSipView.findAll({
                where: {idEmpleado},
            });
            res.json(llamadas);
        } else {
            const usuarioSubarea: any = await usuariosSubarea.findAll({
                where: {idUsuario}
            });
            const subareas: any = [];
            usuarioSubarea.forEach((users: any) => {
                subareas.push(users.idSubarea);
            });
            if (subareas.length === 0) {
                subareas.push(Number(idSubarea));
            }
            const llamadasSubarea = await LlamadaEntrantesSipView.findAll({
                where: {
                    idSubarea: {[Op.in]: subareas}
                },
                order: [
                    ['fechaRegistro', 'DESC']
                ]
            });
            res.json(llamadasSubarea);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const getLlamadasPjsip = async (req: Request, res: Response) => {
    const {idUsuario, idPuesto, idSubarea, idEmpleado} = req.params;
    try {
        if (idPuesto === '8') {
            const llamadas = await LlamadaEntrantesPjsipView.findAll({
                where: {idEmpleado},
                order: [
                    ['fechaRegistro', 'DESC']
                ]
            });
            res.json(llamadas);
        } else {
            const usuarioSubarea: any = await usuariosSubarea.findAll({
                where: {idUsuario}
            });
            const subareas: any = [];
            usuarioSubarea.forEach((users: any) => {
                subareas.push(users.idSubarea);
            });
            if (subareas.length === 0) {
                subareas.push(Number(idSubarea));
            }
            const llamadasSubarea = await LlamadaEntrantesPjsipView.findAll({
                where: {
                    idSubarea: {[Op.in]: subareas}
                },
                order: [
                    ['fechaRegistro', 'DESC']
                ]
            });
            res.json(llamadasSubarea);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};
