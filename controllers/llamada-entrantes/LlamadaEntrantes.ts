import { Request, Response } from 'express';
import LlamadaEntrantes from "../../models/Llamada-Entrantes/llamadaEntrantePjsip";
import moment from "moment-timezone";
import telefoniaView from "../../models/SolicitudesVn/telefoniaView";
import {Op} from "sequelize";
import cdr from "../../models/Grabaciones/cdr";
import LogwebrtcGrabaciones from "../../models/pjsip/logwebrtc/logwebrtcgrabaciones";
import logwebrtcgrabaciones from "../../models/Grabaciones/logwebrtcgrabaciones";
export const inicioLlamadaIb = async (req: Request, res: Response) => {
    const {idEmpleado, numero} = req.body;
  try{
      const llamadaIb = await LlamadaEntrantes.create({
          idTipoLlamada: 3,
          idEstadoLlamada: 4,
          idEmpleado,
          numero,
          fechaInicio: moment().tz('America/Mexico_City').format()
      });
      res.json(llamadaIb);
  }catch (e) {
      res.status(500).json(e);
  }
};


export const setAsteriskIds = async (req: Request, res: Response) => {
    const {numero, idLlamada} = req.params;
    try {
        const logwebrtc: any = await logwebrtcgrabaciones.findOne({
           where: {
               recordingfile: {
                   [Op.like]: `%${numero}%`
               },
           },
            order: [
                ['id', 'DESC']
            ]
        });
        const llamada = await LlamadaEntrantes.findByPk(idLlamada);
        if (!llamada) {
            return res.status(404).json(`No se encontro una llamada con el id: ${idLlamada}`);
        }
        await llamada.update({
            idAsterisk: logwebrtc.uniqueid
        });
        res.json(logwebrtc.uniqueid);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const finLlamadaib = async(req: Request, res: Response) => {
    const {idLlamada} = req.params;
    const {idSolicitud, idEstadoLlamada} = req.body;
  try {
      const llamadaIb = await LlamadaEntrantes.findByPk(idLlamada);
      if (llamadaIb) {
          await llamadaIb.update({
              idSolicitud,
              idEstadoLlamada: idEstadoLlamada || 3,
              fechaFinal: moment().tz('America/Mexico_City').format()
          });
          res.json(llamadaIb);
      } else {
          res.status(404).json(`No existe llamada con el id: ${idLlamada}`);
      }
  }  catch (e) {
      res.status(500).json(e);
  }
};

export const guardarSubetiqueta = async (req: Request, res: Response) => {
    const {idLlamada} = req.params;
    const {idSubEtiqueta} = req.body;
    try{
        const llamadaIb = await LlamadaEntrantes.findByPk(idLlamada);
        if (llamadaIb) {
            await llamadaIb.update({
                idSubEtiqueta,
                finTipificacion: moment().tz('America/Mexico_City').format()
            });
            res.json(llamadaIb);
        } else {
            res.status(404).json(`No existe llamada con el id: ${idLlamada}`);
        }
    }catch (e) {
        res.status(500).json(e);
    }
};

export const getCotizaciones = async (req: Request, res: Response) => {
    const {numero} = req.params;
    try{
        const cotizaciones = await telefoniaView.findAll({
            where: {numero}
        });
        res.json(cotizaciones);
    }catch (e) {
        console.log(e);
        res.status(500).json(e);
    }
};

export const llamadaSinEtiquetaIb = async (req: Request, res: Response) => {
    const {idEmpleado} = req.params;
    try{
        // const llamadaSinCerrar = await LlamadaEntrantes.findAll({
        //    where: {idEmpleado, fechaFinal: null, idEstadoLlamada: 4}
        // });
        // if (llamadaSinCerrar.length > 0) {
        //     llamadaSinCerrar.forEach((llamada) => {
        //         llamada.update({
        //             idEstadoLlamada: 6
        //         });
        //     });
        // }
        const llamadasSinEtiqueta = await LlamadaEntrantes.findAll({
            where: {
                [Op.or]: [
                    {idEmpleado, idSubEtiqueta: null, idEstadoLlamada: 3 },
                    {idEmpleado, idSubEtiqueta: null, idEstadoLlamada: 5 }
                ]
            }
        });
        res.json(llamadasSinEtiqueta);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const buscarSinTerminarIb = async (req: Request, res: Response) => {
    const {idAsterisk} = req.params;
    try {
        const llamadaTerminada = await cdr.findOne({
            where: {
                uniqueid: idAsterisk
            },
        });
        if(llamadaTerminada) {
            const llamadaEntrante = await LlamadaEntrantes.findOne({
               where: {idAsteriskTroncal: idAsterisk, fechaFinal: null}
            });
            if (llamadaEntrante) {
                await llamadaEntrante.update({
                    idEstadoLlamada: 6,
                });
                res.json(1);
            } else {
                res.json(1);
            }
        } else {
            res.json(0);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const sinTerminarIbCDR = async (req: Request, res: Response) => {
  const {idAsterisk} = req.params;
  try {
      const llamadaTerminada = await cdr.findOne({
          where: {
              uniqueid: idAsterisk
          },
      });
      if (llamadaTerminada) {
          res.json(1);
      } else {
          res.json(0);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};

export const llamadaSinIdAsteriskIb = async (req: Request, res: Response) => {
  const {idLlamada} = req.params;
  try {
      const llamadaSinAsterisk = await LlamadaEntrantes.findByPk(idLlamada);
      if (llamadaSinAsterisk) {
          await llamadaSinAsterisk.update({
              idEstadoLlamada: 6,
          });
          res.json(llamadaSinAsterisk);
      } else {
          res.json('fallo');
      }
  }catch (e) {
      res.status(500).json(e);
  }
};

export const llamadaSinFinalizar = async(req: Request, res: Response) => {
  const {idEmpleado} = req.params;
  try{
      const llamadaNoTerminada = await LlamadaEntrantes.findOne({
          where: {idEmpleado, idEstadoLlamada: 4}
      });
      if (llamadaNoTerminada) {
          llamadaNoTerminada.update({
              idEstadoLlamada: 6,
              fechaFinal: moment().tz('America/Mexico_City').format()
          });
      }
      res.json(llamadaNoTerminada);
  } catch (e) {
      res.status(500).json(e);
  }
};
