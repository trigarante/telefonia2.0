import { Request, Response } from 'express';
import callCenter from "../../models/Llamada-Simultanea/callCenter";

export const getCallCenters = async (req: Request, res: Response) => {
    const {idPuesto} = req.params;
  try {
      if (idPuesto === '8') {
          const dataCallCenters = await callCenter.findAll({
             where: {ejecutivo: 1}
          });
          res.json(dataCallCenters);
      } else {
          const dataCallCenters = await callCenter.findAll();
          res.json(dataCallCenters);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};
