import {Request, Response} from "express";
import CurrentServer from "../../models/currentServer/currentServer";

export const getCurrentServer = async (req: Request, res: Response) => {
  try{
      const servidorActual: any = await CurrentServer.findOne({
          where: {
              activo: 1
          }
      });
      res.json(servidorActual.currentServer);
  } catch (e) {
      res.status(500).json(e);
  }
};
