import { Request, Response } from 'express';
import empleadoView from "../../models/Empleado/EmpleadoView";
import usuarios from "../../models/Usuarios/usuarios";
import usuariosSubarea from "../../models/Usuarios/usuariosSubarea";
import enLlamadaSalida from "../../models/Supervisores/enLlamadaSalida";
import {Op} from "sequelize";
import enLlamadaEntrante from "../../models/Supervisores/enLlamadaEntrante";
import usuariosLineaView from "../../models/Supervisores/ejecutivosLinea";
import sessionUsuariosPausaView from "../../models/Supervisores/ejecutivosPausa";
import empleadoLlamadasSalidasView from "../../models/Supervisores/callbackLlamadasTotales";
import numerosRegistro from "../../models/Agregar-Numero-idRegistro/numerosRegistro";
import llamadaSalida from "../../models/Llamada-Salidas/llamadaSalidas";
import llamadaSalidas from "../../models/Llamada-Salidas/llamadaSalidas";
import EnLlamadaOutPjsip from "../../models/pjsip/llamada-salidas/views/EnLlamadaOutPjsip";

export const getenLlamadaSalida = async (req: Request, res: Response) => {
    const { idUsuario, idPuesto, idTipo, idSubarea} = req.params;
  try {
      const usuarioSubarea: any = await usuariosSubarea.findAll({
          where: {idUsuario}
      });
      const subareas: any = [];
      if (idPuesto === '7' || (idPuesto === '8' && idTipo === '2') || idPuesto === '5') {
          usuarioSubarea.forEach((users: any) => {
              subareas.push(users.idSubarea);
          });

          if (subareas.length === 0){
              subareas.push(Number(idSubarea));
          }

          const llamadasOut = await enLlamadaSalida.findAll({
              where: {
                  idSubarea: {[Op.in]: subareas}
              },
              order: [
                  ['fechaRegistro', 'ASC']
              ]
          });
          res.json(llamadasOut);
      } else {
          const llamadasOut = await enLlamadaSalida.findAll();
          res.json(llamadasOut);
      }
      res.json(idPuesto);
  } catch (e) {
      console.log(e);
      res.status(500).json(e);
  }
};

export const getenLlamadaEntrada = async (req: Request, res: Response) => {
    const { idUsuario, idPuesto, idTipo, idSubarea} = req.params;
    try {
        const usuarioSubarea: any = await usuariosSubarea.findAll({
            where: {idUsuario}
        });
        const subareas: any = [];
        if (idPuesto === '7' || (idPuesto === '8' && idTipo === '2') || idPuesto === '5') {
            usuarioSubarea.forEach((users: any) => {
                subareas.push(users.idSubarea);
            });
            if (subareas.length === 0){
                subareas.push(Number(idSubarea));
            }
            const llamadasEntrantes = await enLlamadaEntrante.findAll({
                where: {
                    idSubarea: {[Op.in]: subareas}
                },
                order: [
                    ['fechaRegistro', 'ASC']
                ]
            });
            res.json(llamadasEntrantes);
        } else {
            const llamadasEntrantes = await enLlamadaEntrante.findAll();
            res.json(llamadasEntrantes);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const enLinea = async (req: Request, res: Response) => {
    try {
        const {idUsuario} = req.params;
        const empleado: any = await empleadoView.findOne({
            where: {idUsuario}
        });
        const usuario: any = await usuarios.findOne({
            where: {id: idUsuario}
        });
        const usuarioSubarea: any = await usuariosSubarea.findAll({
            where: {idUsuario}
        });
        const subareas: any = [];
        if (empleado.idPuesto === 7 || (empleado.idPuesto === 8 && usuario.idTipo === 2) || empleado.idPuesto === 5) {
            // tslint:disable-next-line:no-shadowed-variable
            usuarioSubarea.forEach((usuarios: any) => {
                subareas.push(usuarios.idSubarea);
            });
            if(subareas.length === 0){
                subareas.push(empleado.idSubarea);
            }
            const usuariosLinea = await usuariosLineaView.findAll({
                where: {
                    idSubarea: {[Op.in]: subareas}
                }
            });
            res.json(usuariosLinea);
        } else {
            const usuariosLinea = await usuariosLineaView.findAll();
            res.json(usuariosLinea);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const enPausa = async (req: Request, res: Response) => {
    try {
        const {idUsuario} = req.params;
        const empleado: any = await empleadoView.findOne({
            where: {idUsuario}
        });
        const usuario: any = await usuarios.findOne({
            where: {id: idUsuario}
        });
        const usuarioSubarea: any = await usuariosSubarea.findAll({
            where: {idUsuario}
        });
        const subareas: any = [];
        if (empleado.idPuesto === 7 || (empleado.idPuesto === 8 && usuario.idTipo === 2) || empleado.idPuesto === 5) {
            // tslint:disable-next-line:no-shadowed-variable
            usuarioSubarea.forEach((usuarios: any) => {
                subareas.push(usuarios.idSubarea);
            });
            if(subareas.length === 0){
                subareas.push(empleado.idSubarea);
            }
            const usuariosPausa = await sessionUsuariosPausaView.findAll({
                where: {
                    idSubarea: {[Op.in]: subareas}
                }
            });
            res.json(usuariosPausa);
        } else {
            const usuariosPausa = await sessionUsuariosPausaView.findAll();
            res.json(usuariosPausa);
        }
    } catch (e) {
        res.status(500).json(e);
    }
};

export const agregarNumeroRegistro = async (req: Request, res: Response) => {
    const {idRegistro, numero, idSolicitud} = req.body;
  try {
      const nuevoNumero = await numerosRegistro.create({
         idRegistro,
         numero,
          idSolicitud,
      });
      res.json(nuevoNumero);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getNumerosByIdRegistro = async (req: Request, res: Response) => {
  const {idRegistro} = req.params;
  try {
      const numeros = await numerosRegistro.findAll({
         where: {idRegistro},
      });
      res.json(numeros);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const getNumerosByIdSolicitud = async (req: Request, res: Response) => {
    const {idSolicitud} = req.params;
    try {
        const numeros = await numerosRegistro.findAll({
            where: {idSolicitud},
        });
        res.json(numeros);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const callbackRealizado = async (req: Request, res: Response) => {
    try {
        const empleado = req.params.empleado;
        const result: any = await llamadaSalida.findAll({
            where: {
                idEmpleado: empleado,
                [Op.or]:[
                    {idEstadoLlamada: 3},
                    {idEstadoLlamada: 5},
                    {idEstadoLlamada: 6}
                ],
                idTipoLlamada: 2,
            }
        });
        res.json(result);
    } catch (e) {
        res.status(500).json(e);
    }
};
export const empleadoCallback = async (req: Request, res: Response) => {
    const empleado = req.params.idEmpleado;
    const llamada = req.params.idLlamada;
    try{
        const llamadaCallback = await llamadaSalidas.findByPk(llamada);
        if (llamadaCallback) {
            await llamadaCallback.update({
                idEmpleado: empleado,
            });
            res.json(llamadaCallback);
        } else {
            res.status(404).json(`No existe empleado con id ${empleado}`);
        }
    }  catch (e) {
        res.status(500).json(e);
    }
};
export const getEmpleadosCallback = async (req: Request, res: Response) => {
    try {
        const idUsuario = req.params.id;
        const idPuesto = req.params.idPuesto;
        const idTipo = req.params.idTipo;
        const idSubarea = req.params.idSubarea;

        const usuarioSubarea: any = await usuariosSubarea.findAll({
            where: {idUsuario}
        });
        const subareas: any = [];
        if (Number(idPuesto) === 7 || Number(idPuesto) === 8 && Number(idTipo) === 2) {
            usuarioSubarea.forEach((usuari: any) => {
                subareas.push(usuari.idSubarea);
            });
            if(subareas.length === 0){
                subareas.push(idSubarea);
            }
            const llamadasTotales = await empleadoLlamadasSalidasView.findAll({
                where: {
                    idSubarea: {[Op.in]: subareas}
                }
            });
            res.json(llamadasTotales);
        } else {
            const llamadasTotales = await empleadoLlamadasSalidasView.findAll();
            res.json(llamadasTotales);
        }
    } catch (e) {
        console.log(e);
        res.status(500).json(e);

    }
};
export const getCallbackEmpleado = async (req: Request, res: Response) => {
    try {
        const empleado = req.params.empleado;
        const result: any = await llamadaSalida.findAll({
            where: {
                idEmpleado: empleado,
                [Op.or]:[
                    {idEstadoLlamada: 3},
                    {idEstadoLlamada: 5},
                    {idEstadoLlamada: 6},
                    {idEstadoLlamada: 2},
                ],
                idTipoLlamada: 2,
            },
            order: [
                ['fechaRegistro','DESC']
            ],
        });
        res.json(result);
    } catch (e) {
        res.status(500).json(e);
    }
};

export const enLlamadaSalidaPjsip = async (req: Request, res: Response) => {
    const { idUsuario, idPuesto, idTipo, idSubarea} = req.params;
  try {
      const usuarioSubarea: any = await usuariosSubarea.findAll({
          where: {idUsuario}
      });

      const subareas: any = [];
      if (idPuesto === '7' || (idPuesto === '8' && idTipo === '2')) {
          usuarioSubarea.forEach((users: any) => {
              subareas.push(users.idSubarea);
          });

          if (subareas.length === 0){
              subareas.push(Number(idSubarea));
          }
          const llamadasOut = await EnLlamadaOutPjsip.findAll({
              where: {
                  idSubarea: {[Op.in]: subareas}
              },
              order: [
                  ['fechaInicio', 'ASC']
              ]
          });
          res.json(llamadasOut);
      } else {
          const llamadasOut = await EnLlamadaOutPjsip.findAll();
          res.json(llamadasOut);
      }
  } catch (e) {
      res.status(500).json(e);
  }
};
