import { Request, Response } from 'express';
import speechView from "../../models/Speech/view/speechView";
import speech from "../../models/Speech/speech";
import moment from "moment-timezone";
export const getSpeechCreados = async (req: Request, res: Response) => {
  try{
      const speechs = await speechView.findAll({
          where: {
              idEstadoSpeech: 2
          },
          order: [
              ['fechaCreacion', 'DESC']
          ]
      });
      res.json(speechs)
  }catch (e) {
      res.status(500).json(e);
  }
};

export const crearSpeech = async (req: Request, res: Response) => {
    const {body} = req;
  try {
      const nuevoSpeech = await speech.create({
          idSubArea: body.idSubArea,
          speech: body.speech,
          idEmpleadoCreador: body.idEmpleadoCreador,
          titulo: body.titulo,
          idEstadoSpeech: 1,
          fechaCreacion: moment().tz('America/Mexico_City').format(),
      });
      res.json(nuevoSpeech);
  } catch (e) {
      res.status(500).json(e);
  }
};

export const speechPorAutorizar = async (req: Request, res: Response) => {
  try{
      const speechPorAutoriar = await speechView.findAll({
         where: {idEstadoSpeech: 1}
      });
      res.json(speechPorAutoriar);
  }catch (e) {
      res.status(500).json(e);
  }
};

export const getSpeechById = async (req: Request, res: Response) => {
    const {idSpeech} = req.params;
    try {
    const uniqueSpeech = await speechView.findByPk(idSpeech);
        res.json(uniqueSpeech);
    }catch (e) {
        res.status(500).json(e);
    }
};

export const getSpeechInactivos = async (req: Request, res: Response) => {
    try{
        const speechs = await speechView.findAll({
            where: {
                idEstadoSpeech: 3
            },
            order: [
                ['fechaCreacion', 'DESC']
            ]
        });
        res.json(speechs)
    }catch (e) {
        res.status(500).json(e);
    }
};

export const getSpeechAutorizado = async (req: Request, res: Response) => {
    try{
        const speechs = await speechView.findAll({
            where: {
                idEstadoSpeech: 2
            },
            order: [
                ['fechaCreacion', 'DESC']
            ]
        });
        res.json(speechs)
    }catch (e) {
        res.status(500).json(e);
    }
};

export const bajaSpeech = async (req: Request, res: Response) => {
    const idSpeech = req.params.idSpeech;
    try {
        const uniqueSpeech = await speech.findByPk(idSpeech);
        if (uniqueSpeech) {
            await uniqueSpeech.update({
                idEstadoSpeech: 3,
                fechaInhabilitacion: moment().tz('America/Mexico_City').format()
            });
            res.json(uniqueSpeech);
        } else {
            res.status(404).json(`No speech con id ${idSpeech}`);
        }
    }catch (e) {
        res.status(500).json(e);
    }
};

export const putSpeech = async (req: Request, res: Response) => {
    const idSpeech = req.body.id;
    const _titulo = req.body.titulo;
    const _speech = req.body.speech;
    const _idSubArea = req.body.idSubArea;
    try {
        const uniqueSpeech = await speech.findByPk(idSpeech);
        if (uniqueSpeech) {
            await uniqueSpeech.update({
                titulo: _titulo,
                speech: _speech,
                idSubArea: _idSubArea
            });
            res.json(uniqueSpeech);
        } else {
            res.status(404).json(`No speech con id ${idSpeech}`);
        }
    }catch (e) {
        res.status(500).json(e);
    }
};

export const getSpeechBySubarea = async (req: Request, res: Response) => {
    const _idSubarea = req.params.idSubarea;
    try{
        const speechs = await speech.findAll({
            where: {
                idSubArea: _idSubarea
            }
        });
        res.json(speechs)
    }catch (e) {
        res.status(500).json(e);
    }
};

export const putAutorizar = async (req: Request, res: Response) => {
    const idSpeech = req.body.id;
    const _idEstadoSpeech = req.body.idEstadoSpeech;
    const _idEmpleadoAtorizo = req.body.idEmpleadoAtorizo;
    try {
        const uniqueSpeech = await speech.findByPk(idSpeech);
        const fechaActual = new Date();
        if (uniqueSpeech) {
            await uniqueSpeech.update({
                idEstadoSpeech: _idEstadoSpeech,
                idEmpleadoAtorizo: _idEmpleadoAtorizo,
                fechaAutorizacion: fechaActual
            });
            res.json(uniqueSpeech);
        } else {
            res.status(404).json(`No speech con id ${idSpeech}`);
        }
    }catch (e) {
        res.status(500).json(e);
    }
};

export const putDesautorizar = async (req: Request, res: Response) => {
    const idSpeech = req.body.id;
    const _idEstadoSpeech = req.body.idEstadoSpeech;
    const _idEmpleadoAtorizo = req.body.idEmpleadoAtorizo;
    try {
        const uniqueSpeech = await speech.findByPk(idSpeech);
        const fechaActual = new Date();
        if (uniqueSpeech) {
            await uniqueSpeech.update({
                idEstadoSpeech: _idEstadoSpeech,
                idEmpleadoAtorizo: _idEmpleadoAtorizo,
                fechaDesautorizacion: fechaActual
            });
            res.json(uniqueSpeech);
        } else {
            res.status(404).json(`No speech con id ${idSpeech}`);
        }
    }catch (e) {
        res.status(500).json(e);
    }
};

export const putInhabilitar = async (req: Request, res: Response) => {
    const idSpeech = req.body.id;
    const _idEstadoSpeech = req.body.idEstadoSpeech;
    const _idEmpleadoAtorizo = req.body.idEmpleadoAtorizo;
    try {
        const uniqueSpeech = await speech.findByPk(idSpeech);
        const fechaActual = new Date();
        if (uniqueSpeech) {
            await uniqueSpeech.update({
                idEstadoSpeech: _idEstadoSpeech,
                idEmpleadoAtorizo: _idEmpleadoAtorizo,
                fechaAutorizacion: fechaActual
            });
            res.json(uniqueSpeech);
        } else {
            res.status(404).json(`No speech con id ${idSpeech}`);
        }
    }catch (e) {
        res.status(500).json(e);
    }
};

export const getSpeechInhabilitados = async (req: Request, res: Response) => {
    try{
        const speechs = await speechView.findAll({
            where: {
                idEstadoSpeech: 3
            },
            order: [
                ['fechaCreacion', 'DESC']
            ]
        });
        res.json(speechs)
    }catch (e) {
        res.status(500).json(e);
    }
};